﻿using BiogasStationManagement.Models.Data;
using BiogasStationManagement.Models.Storage;
using BiogasStationManagement.ViewModels.Commands.Storage;
using BiogasStationManagement.ViewModels.Managers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BiogasStationManagement.ViewModels.ModelVMs
{
    internal class IngredientVM : INotifyPropertyChanged
    {
        private IEnumerable<Trough> allTroughs;
        private IEnumerable<Trough> availableTroughs;

        private Trough selectedTrough;

        public IngredientVM(IEnumerable<Trough> allTroughs, BatchVM batch, PermissionManager permissionManager)
        {
            Ingredient = new Ingredient();
            this.allTroughs = allTroughs;
            RemoveIngredientCommand = new RemoveIngredientCommand(batch, this, permissionManager);
        }

        public IngredientVM(Ingredient ingredient, IEnumerable<Trough> allTroughs, BatchVM batch, PermissionManager permissionManager)
        {
            Ingredient = ingredient;
            this.allTroughs = allTroughs;
            SelectedTrough = allTroughs.Single(x => x.TroughIndex == ingredient.TroughIndex);
            RemoveIngredientCommand = new RemoveIngredientCommand(batch, this, permissionManager);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public IEnumerable<Trough> AllTroughs { get { return allTroughs; } }

        public double Amount
        {
            get { return Ingredient.Amount; }
            set
            {
                Ingredient.Amount = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Amount"));
            }
        }

        public IEnumerable<Trough> AvailableTroughs
        {
            get { return availableTroughs; }
            set { availableTroughs = value; }
        }

        public Ingredient Ingredient { get; private set; }
        public ICommand RemoveIngredientCommand { get; private set; }

        public Trough SelectedTrough
        {
            get { return selectedTrough; }
            set
            {
                if (selectedTrough == value)
                    return;
                selectedTrough = value;
                if (Ingredient.TroughIndex != selectedTrough.TroughIndex)
                    Ingredient.TroughIndex = selectedTrough.TroughIndex;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SelectedTrough"));
            }
        }

        public int TroughIndex
        {
            get { return Ingredient.TroughIndex; }
            set
            {
                if (Ingredient.TroughIndex == value)
                    return;
                Ingredient.TroughIndex = value;
                selectedTrough = allTroughs.Single(x => x.TroughIndex == value);
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("TroughIndex"));
            }
        }

        public void RefreshTroughs(IEnumerable<Trough> selectedTroughs)
        {
            var temp = allTroughs.Where(x => !selectedTroughs.Contains(x) || x == selectedTrough);
            AvailableTroughs = temp;
        }
    }
}