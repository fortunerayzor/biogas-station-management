﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using BiogasStationManagement.Models.Checks;
using BiogasStationManagement.Models.Data;
using BiogasStationManagement.ViewModels.Commands.Checks.Engine;
using BiogasStationManagement.ViewModels.Managers;

namespace BiogasStationManagement.ViewModels.ModelVMs
{
    internal class EngineCheckHistoryVM : IDataIntegrity, INotifyPropertyChanged
    {
        private EngineCheckHistory engineCheckHistory;
        private CollectionViewSource checksViewSource;
        private EngineCheck newEngineCheck;
        private double remainingHours;

        public EngineCheckHistoryVM(CheckManager checkManager, PermissionManager permissionManager)
        {
            EngineCheckHistory = new EngineCheckHistory();
            SharedInitialization();
            InitializeCommands(checkManager, permissionManager);
        }

        public EngineCheckHistoryVM(CheckManager checkManager, EngineCheckHistory engineCheckHistory, PermissionManager permissionManager)
        {
            EngineCheckHistory = engineCheckHistory;
            SharedInitialization();
            InitializeCommands(checkManager, permissionManager);
        }

        public event DataChangedHandler DataChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        public bool AddIntervalFromLastCheck
        {
            get
            {
                return EngineCheckHistory.AddIntervalFromLastCheck;
            }
            set
            {
                if (EngineCheckHistory.AddIntervalFromLastCheck == value)
                    return;
                EngineCheckHistory.AddIntervalFromLastCheck = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AddIntervalFromLastCheck"));
            }
        }

        public object DataChangedBeacon { get; private set; }

        public EngineCheckHistory EngineCheckHistory
        {
            get
            {
                return engineCheckHistory;
            }
            set
            {
                engineCheckHistory = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("EngineCheckHistory"));
            }
        }

        public SelectableCollection<EngineCheck> EngineChecks
        {
            get { return EngineCheckHistory.EngineChecks; }
        }

        public int EngineIndex
        {
            get
            {
                return EngineCheckHistory.EngineIndex;
            }
            set
            {
                if (EngineCheckHistory.EngineIndex == value)
                    return;
                EngineCheckHistory.EngineIndex = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("EngineIndex"));
            }
        }

        public string HeaderString
        {
            get
            {
                if (remainingHours < 0)
                    return string.Format("{0}, motor KJ{1}, přesáhnuto o: {2} mth", Name, EngineIndex, -RemainingHours);
                return string.Format("{0}, motor KJ{1}, zbývá: {2} mth", Name, EngineIndex, RemainingHours);
            }
        }

        public ICollectionView ChecksView { get; private set; }

        public string Name
        {
            get
            {
                return EngineCheckHistory.Name;
            }
            set
            {
                if (EngineCheckHistory.Name == value)
                    return;
                EngineCheckHistory.Name = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Name"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("HeaderString"));
            }
        }

        public EngineCheck NewEngineCheck
        {
            get
            {
                return newEngineCheck;
            }
            set
            {
                newEngineCheck = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("NewEngineCheck"));
            }
        }

        public double RemainingHours
        {
            get
            {
                return remainingHours;
            }
            set
            {
                remainingHours = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("RemainingHours"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("HeaderString"));
            }
        }

        public double ServiceInterval
        {
            get
            {
                return EngineCheckHistory.ServiceInterval;
            }
            set
            {
                if (EngineCheckHistory.ServiceInterval == value)
                    return;
                EngineCheckHistory.ServiceInterval = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ServiceInterval"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("HeaderString"));
            }
        }

        public static explicit operator EngineCheckHistory(EngineCheckHistoryVM engineCheckHistoryVM)
        {
            return engineCheckHistoryVM.EngineCheckHistory;
        }

        public void RefreshRemainingHours(double? latestLoggedHours)
        {
            if (latestLoggedHours.HasValue)
            {
                if (EngineChecks.Count == 0)
                {
                    RemainingHours = ServiceInterval - latestLoggedHours.Value;
                }
                else if (AddIntervalFromLastCheck)
                {
                    RemainingHours = EngineChecks.OrderBy(x => x.Timestamp).First().EngineHours + ServiceInterval - latestLoggedHours.Value;
                }
                else
                {
                    RemainingHours = EngineChecks.OrderBy(x => x.Timestamp).First().EngineHours.RoundToNearest(ServiceInterval) + ServiceInterval - latestLoggedHours.Value;
                }
            }
            else
            {
                RemainingHours = ServiceInterval;
            }
        }

        private void EngineCheckHistory_DataChanged(DataChangedEventArgs e)
        {
            if (e.ChildEventArgs != null)
            {
                if (e.ChildEventArgs.Flag == DataChangedFlag.Added || e.ChildEventArgs.Flag == DataChangedFlag.Removed)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("DataChangedBeacon"));
            }
            DataChanged?.Invoke(new DataChangedEventArgs(this, e));
        }

        private void InitializeCommands(CheckManager checkManager, PermissionManager permissionManager)
        {
            DeleteEngineCheckHistoryCommand = new DeleteEngineCheckHistoryCommand(checkManager, permissionManager);
            DeleteEngineCheckCommand = new DeleteEngineCheckCommand(this, permissionManager);
            CreateNewEngineCheckCommand = new CreateNewEngineCheckCommand(this, permissionManager);
            InsertNewEngineCheckCommand = new InsertNewEngineCheckCommand(this, permissionManager);
            DeleteNewEngineCheckCommand = new DeleteNewEngineCheckCommand(this, permissionManager);
        }

        private void SharedInitialization()
        {
            EngineCheckHistory.DataChanged += EngineCheckHistory_DataChanged;
            EngineChecks.DebugSubscription = true;
            checksViewSource = new CollectionViewSource() { Source = EngineCheckHistory.EngineChecks };
            ChecksView = checksViewSource.View;
            ChecksView.EnableLiveSorting("Timestamp", ListSortDirection.Descending);
        }

        #region Commands

        public ICommand CreateNewEngineCheckCommand { get; private set; }
        public ICommand DeleteEngineCheckCommand { get; private set; }
        public ICommand DeleteEngineCheckHistoryCommand { get; private set; }
        public ICommand DeleteNewEngineCheckCommand { get; private set; }
        public ICommand InsertNewEngineCheckCommand { get; private set; }

        #endregion Commands
    }
}