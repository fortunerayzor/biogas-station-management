﻿using BiogasStationManagement.Models.Data;
using BiogasStationManagement.Models.Storage;
using BiogasStationManagement.ViewModels.Commands.Storage;
using BiogasStationManagement.ViewModels.Managers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BiogasStationManagement.ViewModels.ModelVMs
{
    internal class BatchVM : INotifyPropertyChanged, IDataIntegrity
    {
        private Batch batch;
        private ObservableCollection<IngredientVM> ingredients;

        public BatchVM(DateTime timestamp, IEnumerable<Trough> troughs, PermissionManager permissionManager)
        {
            Batch = new Batch(timestamp);
            Batch.DataChanged += Batch_DataChanged;

            Ingredients = new ObservableCollection<IngredientVM>();
            Ingredients.CollectionChanged += Ingredients_CollectionChanged;
            Ingredients.Add(new IngredientVM(troughs, this, permissionManager));

            Timestamp = timestamp;
            TroughCount = troughs.Count();

            AddIngredientCommand = new AddIngredientCommand(this, permissionManager);
        }

        /// <summary>
        /// Constructor used during deserialization.
        /// </summary>
        /// <param name="batch">Deserialized batch from dataservice.</param>
        /// <param name="troughs">All troughs used used for selection in views.</param>
        /// <param name="permissionManager">Manadtory permission manager for storage commands.</param>
        public BatchVM(Batch batch, IEnumerable<Trough> troughs, PermissionManager permissionManager)
        {
            Batch = batch;
            Batch.DataChanged += Batch_DataChanged;
            TroughCount = troughs.Count();

            Ingredients = new ObservableCollection<IngredientVM>();
            foreach (var item in batch.Ingredients)
            {
                Ingredients.Add(new IngredientVM(item, troughs, this, permissionManager));
            }
            Ingredients.CollectionChanged += Ingredients_CollectionChanged;
            RefreshAvailableTroughs();
            // commands initialization
            AddIngredientCommand = new AddIngredientCommand(this, permissionManager);
        }

        private void Ingredients_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                var ingredientVM = e.NewItems[0] as IngredientVM;
                Batch.Ingredients.Add(ingredientVM.Ingredient);
                ingredientVM.PropertyChanged += Ingredient_PropertyChanged;
                RefreshAvailableTroughs(ingredientVM);
            }
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                var ingredientVM = e.OldItems[0] as IngredientVM;
                Batch.Ingredients.Remove(ingredientVM.Ingredient);
                ingredientVM.PropertyChanged -= Ingredient_PropertyChanged;
                RefreshAvailableTroughs(ingredientVM);
            }
        }

        public event DataChangedHandler DataChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand AddIngredientCommand { get; private set; }

        public Batch Batch
        {
            get { return batch; }
            set
            {
                batch = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Batch"));
            }
        }

        public ObservableCollection<IngredientVM> Ingredients
        {
            get { return ingredients; }
            set
            {
                ingredients = value; // DataChanged is subscribed in ctor
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Ingredients"));
            }
        }

        public DateTime Timestamp
        {
            get { return Batch.Timestamp; }
            set
            {
                Batch.Timestamp = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Timestamp"));
            }
        }

        public int TroughCount { get; internal set; }

        public static explicit operator Batch(BatchVM batchVM)
        {
            return batchVM.Batch;
        }

        private void Batch_DataChanged(DataChangedEventArgs e)
        {
            DataChanged?.Invoke(new DataChangedEventArgs(this, e));
        }

        private void Ingredient_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedTrough")
            {
                RefreshAvailableTroughs();
            }
        }

        private void RefreshAvailableTroughs()
        {
            foreach (IngredientVM item in Ingredients)
            {
                RefreshAvailableTroughs(item);
            }
        }

        private void RefreshAvailableTroughs(IngredientVM ingredientVM)
        {
            var selectedTroughs = Ingredients.Select(x => x.SelectedTrough);
            ingredientVM.RefreshTroughs(selectedTroughs);
        }
    }
}