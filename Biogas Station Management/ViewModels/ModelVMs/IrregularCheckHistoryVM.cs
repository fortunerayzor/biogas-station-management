﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using BiogasStationManagement.Models.Checks;
using BiogasStationManagement.Models.Data;
using BiogasStationManagement.ViewModels.Commands.Checks.Irregular;
using BiogasStationManagement.ViewModels.Managers;

namespace BiogasStationManagement.ViewModels.ModelVMs
{
    internal class IrregularCheckHistoryVM : IDataIntegrity, INotifyPropertyChanged
    {
        private CollectionViewSource checksViewSource;
        private IrregularCheckHistory irregularCheckHistory;
        private Check newCheck;

        public IrregularCheckHistoryVM(CheckManager checkManager, PermissionManager permissionManager)
        {
            IrregularCheckHistory = new IrregularCheckHistory();
            SharedInitialization();
            InitializeCommands(checkManager, permissionManager);
        }

        public IrregularCheckHistoryVM(CheckManager checkManager, IrregularCheckHistory irregularCheck, PermissionManager permissionManager)
        {
            IrregularCheckHistory = irregularCheck;
            SharedInitialization();
            InitializeCommands(checkManager, permissionManager);
        }

        public event DataChangedHandler DataChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        public string HeaderString
        {
            get
            {
                if (Checks.Count > 0)
                    return string.Format("{0}, poslední kontrola: {1}", Name, Checks.OrderBy(x => x.Timestamp).Last().Timestamp);
                return string.Format("{0}", Name);
            }
        }

        public ICollectionView ChecksView { get; private set; }

        public IrregularCheckHistory IrregularCheckHistory
        {
            get
            {
                return irregularCheckHistory;
            }
            set
            {
                irregularCheckHistory = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IrregularCheckHistory"));
            }
        }

        public string Name
        {
            get
            {
                return IrregularCheckHistory.Name;
            }
            set
            {
                if (IrregularCheckHistory.Name == value)
                    return;
                IrregularCheckHistory.Name = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Name"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("HeaderString"));
            }
        }

        public Check NewCheck
        {
            get
            {
                return newCheck;
            }
            set
            {
                newCheck = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("NewCheck"));
            }
        }

        internal SelectableCollection<Check> Checks
        {
            get { return IrregularCheckHistory.Checks; }
        }

        public static explicit operator IrregularCheckHistory(IrregularCheckHistoryVM irregularCheckHistoryVM)
        {
            return irregularCheckHistoryVM.IrregularCheckHistory;
        }

        private void InitializeCommands(CheckManager checkManager, PermissionManager permissionManager)
        {
            DeleteIrregularCheckHistoryCommand = new DeleteIrregularCheckHistoryCommand(checkManager, permissionManager);
            DeleteCheckCommand = new DeleteCheckCommand(this, permissionManager);
            CreateNewCheckCommand = new CreateNewCheckCommand(this, permissionManager);
            InsertNewCheckCommand = new InsertNewCheckCommand(this, permissionManager);
            DeleteNewCheckCommand = new DeleteNewCheckCommand(this, permissionManager);
        }

        private void IrregularCheckHistory_DataChanged(DataChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("HeaderString"));
            DataChanged?.Invoke(new DataChangedEventArgs(this, e));
        }

        private void SharedInitialization()
        {
            IrregularCheckHistory.DataChanged += IrregularCheckHistory_DataChanged;
            Checks.DebugSubscription = true;
            checksViewSource = new CollectionViewSource() { Source = IrregularCheckHistory.Checks };
            ChecksView = checksViewSource.View;
            ChecksView.EnableLiveSorting("Timestamp", ListSortDirection.Descending);
        }

        #region Commands

        public ICommand CreateNewCheckCommand { get; private set; }
        public ICommand DeleteCheckCommand { get; private set; }
        public ICommand DeleteIrregularCheckHistoryCommand { get; private set; }
        public ICommand DeleteNewCheckCommand { get; private set; }
        public ICommand InsertNewCheckCommand { get; private set; }

        #endregion Commands
    }
}