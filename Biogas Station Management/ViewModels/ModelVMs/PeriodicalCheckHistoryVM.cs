﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using BiogasStationManagement.Models.Checks;
using BiogasStationManagement.Models.Data;
using BiogasStationManagement.ViewModels.Commands.Checks.Periodical;
using BiogasStationManagement.ViewModels.Managers;

namespace BiogasStationManagement.ViewModels.ModelVMs
{
    internal class PeriodicalCheckHistoryVM : IDataIntegrity, INotifyPropertyChanged
    {
        private CollectionViewSource checksViewSource;
        private Check newCheck;
        private PeriodicalCheckHistory periodicalCheckHistory;

        private TimeSpan remainingTime;

        public PeriodicalCheckHistoryVM(CheckManager checkManager, PermissionManager permissionManager)
        {
            PeriodicalCheckHistory = new PeriodicalCheckHistory();
            SharedInitialization();
            InitializeCommands(checkManager, permissionManager);
        }

        public PeriodicalCheckHistoryVM(CheckManager checkManager, PeriodicalCheckHistory periodicalCheckHistory, PermissionManager permissionManager)
        {
            PeriodicalCheckHistory = periodicalCheckHistory;
            SharedInitialization();
            InitializeCommands(checkManager, permissionManager);
        }

        public event DataChangedHandler DataChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        public DateTime FirstCheck
        {
            get
            {
                return PeriodicalCheckHistory.FirstCheck;
            }
            set
            {
                if (PeriodicalCheckHistory.FirstCheck == value)
                    return;
                PeriodicalCheckHistory.FirstCheck = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FirstCheck"));
            }
        }

        public string HeaderString
        {
            get
            {
                if (remainingTime.TotalDays < 0)
                    return string.Format("{0}, přesáhnuto o: {1} dnů", Name, RemainingTime.Negate().Days);
                return string.Format("{0}, zbývá: {1} dnů", Name, RemainingTime.Days);
            }
        }

        public ICollectionView ChecksView { get; private set; }

        public string Name
        {
            get
            {
                return PeriodicalCheckHistory.Name;
            }
            set
            {
                if (PeriodicalCheckHistory.Name == value)
                    return;
                PeriodicalCheckHistory.Name = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Name"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("HeaderString"));
            }
        }

        public Check NewCheck
        {
            get
            {
                return newCheck;
            }
            set
            {
                newCheck = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("NewCheck"));
            }
        }

        public PeriodicalCheckHistory PeriodicalCheckHistory
        {
            get
            {
                return periodicalCheckHistory;
            }
            set
            {
                periodicalCheckHistory = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("PeriodicalCheckHistory"));
            }
        }

        public TimeSpan Periodicity
        {
            get
            {
                return PeriodicalCheckHistory.Periodicity;
            }
            set
            {
                if (PeriodicalCheckHistory.Periodicity == value)
                    return;
                PeriodicalCheckHistory.Periodicity = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Periodicity"));
                RefreshRemainingTime();
            }
        }

        public TimeSpan RemainingTime
        {
            get
            {
                return remainingTime;
            }
            set
            {
                remainingTime = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("RemainingTime"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("HeaderString"));
            }
        }

        internal SelectableCollection<Check> Checks
        {
            get { return PeriodicalCheckHistory.Checks; }
        }

        public static explicit operator PeriodicalCheckHistory(PeriodicalCheckHistoryVM periodicalCheckHistoryVM)
        {
            return periodicalCheckHistoryVM.PeriodicalCheckHistory;
        }

        public void RefreshRemainingTime()
        {
            if (Checks.Count == 0)
            {
                RemainingTime = FirstCheck - DateTime.Now + Periodicity;
            }
            else
            {
                var lastCheck = Checks.OrderBy(x => x.Timestamp).Last();
                RemainingTime = lastCheck.Timestamp - DateTime.Now + Periodicity;
            }
        }

        private void InitializeCommands(CheckManager checkManager, PermissionManager permissionManager)
        {
            DeletePeriodicalCheckHistoryCommand = new DeletePeriodicalCheckHistoryCommand(checkManager, permissionManager);
            DeleteCheckCommand = new DeleteCheckCommand(this, permissionManager);
            CreateNewCheckCommand = new CreateNewCheckCommand(this, permissionManager);
            InsertNewCheckCommand = new InsertNewCheckCommand(this, permissionManager);
            DeleteNewCheckCommand = new DeleteNewCheckCommand(this, permissionManager);
        }

        private void PeriodicalCheckHistory_DataChanged(DataChangedEventArgs e)
        {
            DataChanged?.Invoke(new DataChangedEventArgs(this, e));
            RefreshRemainingTime();
        }

        private void SharedInitialization()
        {
            PeriodicalCheckHistory.DataChanged += PeriodicalCheckHistory_DataChanged;
            Checks.DebugSubscription = true;
            checksViewSource = new CollectionViewSource() { Source = PeriodicalCheckHistory.Checks };
            ChecksView = checksViewSource.View;
            ChecksView.EnableLiveSorting("Timestamp", ListSortDirection.Descending);
            RefreshRemainingTime();
        }

        #region Commands

        public ICommand CreateNewCheckCommand { get; private set; }
        public ICommand DeleteCheckCommand { get; private set; }
        public ICommand DeleteNewCheckCommand { get; private set; }
        public ICommand DeletePeriodicalCheckHistoryCommand { get; private set; }
        public ICommand InsertNewCheckCommand { get; private set; }

        #endregion Commands
    }
}