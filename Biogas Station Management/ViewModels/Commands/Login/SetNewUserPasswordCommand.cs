﻿using BiogasStationManagement.ViewModels.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BiogasStationManagement.ViewModels.Commands.Login
{
    class SetNewUserPasswordCommand:ICommand
    {
        private LoginManager loginManager;

        public SetNewUserPasswordCommand(LoginManager loginManager)
        {
            this.loginManager = loginManager;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return loginManager.AuthenticatedUser?.IsSystemAdmin == true && loginManager.Users.SelectedItem != null && loginManager.LoginPassword != string.Empty;
        }

        public void Execute(object parameter)
        {
            loginManager.Users.SelectedItem.PasswordHash = loginManager.LoginPassword.GeneratePasswordHash();
            loginManager.LoginPassword = string.Empty;
        }
    }
}
