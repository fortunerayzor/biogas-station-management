﻿using BiogasStationManagement.ViewModels.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BiogasStationManagement.ViewModels.Commands.Login
{
    class DeleteSelectedUserCommand : ICommand
    {
        private LoginManager loginManager;

        public DeleteSelectedUserCommand(LoginManager loginManager)
        {
            this.loginManager = loginManager;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return loginManager.Users.SelectedItem != null && loginManager.AuthenticatedUser?.IsSystemAdmin == true;
        }

        public void Execute(object parameter)
        {
            loginManager.Users.Remove(loginManager.Users.SelectedItem);
        }
    }
}
