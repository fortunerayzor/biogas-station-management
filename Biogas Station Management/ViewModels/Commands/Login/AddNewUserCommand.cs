﻿using BiogasStationManagement.Models;
using BiogasStationManagement.ViewModels.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BiogasStationManagement.ViewModels.Commands.Login
{
    class AddNewUserCommand : ICommand
    {
        private LoginManager loginManager;

        public AddNewUserCommand(LoginManager loginManager)
        {
            this.loginManager = loginManager;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return loginManager.AuthenticatedUser?.IsSystemAdmin == true;
        }

        public void Execute(object parameter)
        {
            User newUser = new Models.User() { Name = "Nový uživatel" };
            loginManager.Users.Add(newUser);
            loginManager.Users.SelectedItem = newUser;
        }
    }
}
