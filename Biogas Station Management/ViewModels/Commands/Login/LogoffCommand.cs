﻿using BiogasStationManagement.ViewModels.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BiogasStationManagement.ViewModels.Commands.Login
{
    internal class LogoffCommand : ICommand
    {
        private LoginManager loginManager;

        public LogoffCommand(LoginManager loginManager)
        {
            this.loginManager = loginManager;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return loginManager.AuthenticatedUser != null;
        }

        public void Execute(object parameter)
        {
            loginManager.LogoffUser();
        }
    }
}