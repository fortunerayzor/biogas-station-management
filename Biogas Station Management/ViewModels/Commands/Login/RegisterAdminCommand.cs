﻿using BiogasStationManagement.Models;
using BiogasStationManagement.ViewModels.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BiogasStationManagement.ViewModels.Commands.Login
{
    internal class RegisterAdminCommand : ICommand
    {
        private LoginManager loginManager;

        public RegisterAdminCommand(LoginManager loginManager)
        {
            this.loginManager = loginManager;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return loginManager.NoUsers && loginManager.ValidLoginCredentials;
        }

        public void Execute(object parameter)
        {
            loginManager.RegisterFirstUser();
        }
    }
}