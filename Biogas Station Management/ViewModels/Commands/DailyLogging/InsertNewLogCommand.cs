﻿using BiogasStationManagement.ViewModels.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BiogasStationManagement.ViewModels.Commands.DailyLogging
{
    internal class InsertNewLogCommand : ICommand
    {
        private LoggingManager loggingManager;
        private PermissionManager permissionManager;

        public InsertNewLogCommand(LoggingManager loggingManager, PermissionManager permissionManager)
        {
            this.loggingManager = loggingManager;
            this.permissionManager = permissionManager;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return permissionManager.LogsPermission.CanWrite && loggingManager.NewLog != null;
        }

        public void Execute(object parameter)
        {
            loggingManager.Logs.Insert(0, loggingManager.NewLog);
            loggingManager.NewLog = null;
        }
    }
}