﻿using BiogasStationManagement.ViewModels.Managers;
using System;
using System.Windows.Input;

namespace BiogasStationManagement.ViewModels.Commands.DailyLogging
{
    internal class CreateNewLogCommand : ICommand
    {
        private LoggingManager loggingManager;
        private PermissionManager permissionManager;

        public CreateNewLogCommand(LoggingManager loggingManager, PermissionManager permissionManager)
        {
            this.loggingManager = loggingManager;
            this.permissionManager = permissionManager;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return permissionManager.LogsPermission.CanWrite && loggingManager.NewLog == null;
        }

        public void Execute(object parameter)
        {
            loggingManager.NewLog = new Models.Logs.LogEntity(DateTime.Now);
        }
    }
}