﻿using BiogasStationManagement.ViewModels.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BiogasStationManagement.ViewModels.Commands.UI
{
    internal class OpenPageCommand : ICommand
    {
        private AppliactionManager applicationManager;

        public OpenPageCommand(AppliactionManager applicationManager)
        {
            this.applicationManager = applicationManager;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            if (parameter != null)
            {
                return GetPageType((string)parameter) != null;
            }
            return false;
        }

        public void Execute(object parameter)
        {
            applicationManager.OpenPage(Activator.CreateInstance(GetPageType((string)parameter)));
        }

        private Type GetPageType(string pageName)
        {
            return Type.GetType("BiogasStationManagement.Views.Pages." + pageName);
        }
    }
}