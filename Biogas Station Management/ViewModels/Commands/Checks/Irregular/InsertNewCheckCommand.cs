﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using BiogasStationManagement.ViewModels.Managers;
using BiogasStationManagement.ViewModels.ModelVMs;

namespace BiogasStationManagement.ViewModels.Commands.Checks.Irregular
{
    class InsertNewCheckCommand : ICommand
    {
        private PermissionManager permissionManager;
        private IrregularCheckHistoryVM irregularCheckHistoryVM;
        public InsertNewCheckCommand(IrregularCheckHistoryVM irregularCheckHistoryVM, PermissionManager permissionManager)
        {
            this.irregularCheckHistoryVM = irregularCheckHistoryVM;
            this.permissionManager = permissionManager;

        }
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return permissionManager.IrregularCheckHistoriesPermission.CanModify && irregularCheckHistoryVM.NewCheck != null;
        }

        public void Execute(object parameter)
        {
            irregularCheckHistoryVM.Checks.Add(irregularCheckHistoryVM.NewCheck);
            irregularCheckHistoryVM.NewCheck = null;
        }
    }
}
