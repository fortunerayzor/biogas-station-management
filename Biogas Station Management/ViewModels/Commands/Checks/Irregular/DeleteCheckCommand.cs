﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using BiogasStationManagement.Models.Checks;
using BiogasStationManagement.ViewModels.Managers;
using BiogasStationManagement.ViewModels.ModelVMs;

namespace BiogasStationManagement.ViewModels.Commands.Checks.Irregular
{
    class DeleteCheckCommand : ICommand
    {
        private PermissionManager permissionManager;
        private IrregularCheckHistoryVM irregularCheckHistoryVM;
        public DeleteCheckCommand(IrregularCheckHistoryVM irregularCheckHistoryVM, PermissionManager permissionManager)
        {
            this.irregularCheckHistoryVM = irregularCheckHistoryVM;
            this.permissionManager = permissionManager;

        }
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return permissionManager.IrregularCheckHistoriesPermission.CanModify;
        }

        public void Execute(object parameter)
        {
            var engineCheck = parameter as Check;
            irregularCheckHistoryVM.Checks.Remove(engineCheck);
        }
    }
}
