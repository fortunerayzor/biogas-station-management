﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using BiogasStationManagement.ViewModels.Managers;

namespace BiogasStationManagement.ViewModels.Commands.Checks.Irregular
{
    class InsertNewIrregularCheckHistoryCommand : ICommand
    {
        private CheckManager checkManager;
        private PermissionManager permissionManager;

        public InsertNewIrregularCheckHistoryCommand(CheckManager checkManager, PermissionManager permissionManager)
        {
            this.checkManager = checkManager;
            this.permissionManager = permissionManager;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return permissionManager.IrregularCheckHistoriesPermission.CanWrite && checkManager.NewIrregularCheckHistory != null;
        }

        public void Execute(object parameter)
        {
            checkManager.IrregularCheckHistories.Add(checkManager.NewIrregularCheckHistory);
            checkManager.NewIrregularCheckHistory = null;
        }
    }
}
