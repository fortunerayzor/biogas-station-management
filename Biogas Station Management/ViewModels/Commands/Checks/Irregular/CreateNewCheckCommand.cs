﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using BiogasStationManagement.ViewModels.Managers;
using BiogasStationManagement.ViewModels.ModelVMs;

namespace BiogasStationManagement.ViewModels.Commands.Checks.Irregular
{
    internal class CreateNewCheckCommand : ICommand
    {
        private IrregularCheckHistoryVM irregularCheckHistory;
        private PermissionManager permissionManager;

        public CreateNewCheckCommand(IrregularCheckHistoryVM irregularCheckHistory, PermissionManager permissionManager)
        {
            this.irregularCheckHistory = irregularCheckHistory;
            this.permissionManager = permissionManager;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return permissionManager.IrregularCheckHistoriesPermission.CanModify && irregularCheckHistory.NewCheck == null;
        }

        public void Execute(object parameter)
        {
            irregularCheckHistory.NewCheck = new Models.Checks.Check() { Timestamp = DateTime.Now.Date };
        }
    }
}