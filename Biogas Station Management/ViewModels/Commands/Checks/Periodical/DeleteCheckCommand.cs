﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using BiogasStationManagement.Models.Checks;
using BiogasStationManagement.ViewModels.Managers;
using BiogasStationManagement.ViewModels.ModelVMs;

namespace BiogasStationManagement.ViewModels.Commands.Checks.Periodical
{
    class DeleteCheckCommand : ICommand
    {
        private PermissionManager permissionManager;
        private PeriodicalCheckHistoryVM periodicalCheckHistoryVM;
        public DeleteCheckCommand(PeriodicalCheckHistoryVM periodicalCheckHistoryVM, PermissionManager permissionManager)
        {
            this.periodicalCheckHistoryVM = periodicalCheckHistoryVM;
            this.permissionManager = permissionManager;

        }
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return permissionManager.PeriodicalCheckHistoriesPermission.CanModify;
        }

        public void Execute(object parameter)
        {
            var engineCheck = parameter as Check;
            periodicalCheckHistoryVM.Checks.Remove(engineCheck);
        }
    }
}
