﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using BiogasStationManagement.ViewModels.Managers;
using BiogasStationManagement.ViewModels.ModelVMs;

namespace BiogasStationManagement.ViewModels.Commands.Checks.Periodical
{
    class CreateNewCheckCommand : ICommand
    {
        private PermissionManager permissionManager;
        private PeriodicalCheckHistoryVM periodicalCheckHistory;
        public CreateNewCheckCommand(PeriodicalCheckHistoryVM periodicalCheckHistory, PermissionManager permissionManager)
        {
            this.periodicalCheckHistory = periodicalCheckHistory;
            this.permissionManager = permissionManager;

        }
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return permissionManager.PeriodicalCheckHistoriesPermission.CanModify && periodicalCheckHistory.NewCheck == null;
        }

        public void Execute(object parameter)
        {
            periodicalCheckHistory.NewCheck = new Models.Checks.Check() { Timestamp = DateTime.Now.Date };
        }
    }
}
