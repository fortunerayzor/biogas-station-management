﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using BiogasStationManagement.ViewModels.Managers;

namespace BiogasStationManagement.ViewModels.Commands.Checks.Periodical
{
    class InsertNewPeriodicalCheckHistoryCommand : ICommand
    {
        private CheckManager checkManager;
        private PermissionManager permissionManager;

        public InsertNewPeriodicalCheckHistoryCommand(CheckManager checkManager, PermissionManager permissionManager)
        {
            this.checkManager = checkManager;
            this.permissionManager = permissionManager;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return permissionManager.PeriodicalCheckHistoriesPermission.CanWrite && checkManager.NewPeriodicalCheckHistory != null;
        }

        public void Execute(object parameter)
        {
            checkManager.PeriodicalCheckHistories.Add(checkManager.NewPeriodicalCheckHistory);
            checkManager.NewPeriodicalCheckHistory = null;
        }
    }
}
