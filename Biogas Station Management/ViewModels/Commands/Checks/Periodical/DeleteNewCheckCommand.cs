﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using BiogasStationManagement.ViewModels.Managers;
using BiogasStationManagement.ViewModels.ModelVMs;

namespace BiogasStationManagement.ViewModels.Commands.Checks.Periodical
{
    class DeleteNewCheckCommand : ICommand
    {
        private PermissionManager permissionManager;
        private PeriodicalCheckHistoryVM periodicalCheckHistoryVM;
        public DeleteNewCheckCommand(PeriodicalCheckHistoryVM periodicalCheckHistoryVM, PermissionManager permissionManager)
        {
            this.periodicalCheckHistoryVM = periodicalCheckHistoryVM;
            this.permissionManager = permissionManager;

        }
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return permissionManager.PeriodicalCheckHistoriesPermission.CanModify && periodicalCheckHistoryVM.NewCheck != null;
        }

        public void Execute(object parameter)
        {
            periodicalCheckHistoryVM.NewCheck = null;
        }
    }
}
