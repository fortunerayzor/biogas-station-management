﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using BiogasStationManagement.ViewModels.Managers;
using BiogasStationManagement.ViewModels.ModelVMs;

namespace BiogasStationManagement.ViewModels.Commands.Checks.Engine
{
    class CreateNewEngineCheckHistoryCommand : ICommand
    {
        private PermissionManager permissionManager;
        private CheckManager checkManager;
        public CreateNewEngineCheckHistoryCommand(CheckManager checkManager, PermissionManager permissionManager)
        {
            this.checkManager = checkManager;
            this.permissionManager = permissionManager;

        }
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return permissionManager.EngineCheckHistoriesPermission.CanWrite && checkManager.NewEngineCheckHistory == null;
        }

        public void Execute(object parameter)
        {
            checkManager.NewEngineCheckHistory = new EngineCheckHistoryVM(checkManager, permissionManager) { EngineIndex = 1 };
        }
    }
}
