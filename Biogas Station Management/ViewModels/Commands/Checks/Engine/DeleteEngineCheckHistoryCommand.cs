﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using BiogasStationManagement.ViewModels.Managers;
using BiogasStationManagement.ViewModels.ModelVMs;

namespace BiogasStationManagement.ViewModels.Commands.Checks.Engine
{
    class DeleteEngineCheckHistoryCommand : ICommand
    {
        private CheckManager checkManager;
        private PermissionManager permissionManager;

        public DeleteEngineCheckHistoryCommand(CheckManager checkManager, PermissionManager permissionManager)
        {
            this.checkManager = checkManager;
            this.permissionManager = permissionManager;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return permissionManager.EngineCheckHistoriesPermission.CanDelete;
        }

        public void Execute(object parameter)
        {
            var engineCheckHistoryVM = parameter as EngineCheckHistoryVM;
            checkManager.EngineCheckHistories.Remove(engineCheckHistoryVM);
        }
    }
}
