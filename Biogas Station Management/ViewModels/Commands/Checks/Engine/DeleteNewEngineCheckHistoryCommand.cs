﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using BiogasStationManagement.ViewModels.Managers;

namespace BiogasStationManagement.ViewModels.Commands.Checks.Engine
{
    class DeleteNewEngineCheckHistoryCommand : ICommand
    {
        private CheckManager checkManager;
        private PermissionManager permissionManager;

        public DeleteNewEngineCheckHistoryCommand(CheckManager checkManager, PermissionManager permissionManager)
        {
            this.checkManager = checkManager;
            this.permissionManager = permissionManager;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return permissionManager.EngineCheckHistoriesPermission.CanWrite && checkManager.NewEngineCheckHistory != null;
        }

        public void Execute(object parameter)
        {
            checkManager.NewEngineCheckHistory = null;
        }
    }
}
