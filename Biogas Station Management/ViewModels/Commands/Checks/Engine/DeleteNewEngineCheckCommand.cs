﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using BiogasStationManagement.ViewModels.Managers;
using BiogasStationManagement.ViewModels.ModelVMs;

namespace BiogasStationManagement.ViewModels.Commands.Checks.Engine
{
    class DeleteNewEngineCheckCommand : ICommand
    {
        private PermissionManager permissionManager;
        private EngineCheckHistoryVM engineCheckHistoryVM;
        public DeleteNewEngineCheckCommand(EngineCheckHistoryVM engineCheckHistoryVM, PermissionManager permissionManager)
        {
            this.engineCheckHistoryVM = engineCheckHistoryVM;
            this.permissionManager = permissionManager;

        }
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return permissionManager.EngineCheckHistoriesPermission.CanModify && engineCheckHistoryVM.NewEngineCheck != null;
        }

        public void Execute(object parameter)
        {
            engineCheckHistoryVM.NewEngineCheck = null;
        }
    }
}
