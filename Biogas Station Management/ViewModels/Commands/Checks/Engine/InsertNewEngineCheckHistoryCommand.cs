﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using BiogasStationManagement.ViewModels.Managers;

namespace BiogasStationManagement.ViewModels.Commands.Checks.Engine
{
    class InsertNewEngineCheckHistoryCommand : ICommand
    {
        private CheckManager checkManager;
        private PermissionManager permissionManager;

        public InsertNewEngineCheckHistoryCommand(CheckManager checkManager, PermissionManager permissionManager)
        {
            this.checkManager = checkManager;
            this.permissionManager = permissionManager;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return permissionManager.EngineCheckHistoriesPermission.CanWrite && checkManager.NewEngineCheckHistory != null;
        }

        public void Execute(object parameter)
        {
            checkManager.EngineCheckHistories.Add(checkManager.NewEngineCheckHistory);
            checkManager.RefreshEngineRemainingHours(checkManager.NewEngineCheckHistory);
            checkManager.NewEngineCheckHistory = null;
        }
    }
}
