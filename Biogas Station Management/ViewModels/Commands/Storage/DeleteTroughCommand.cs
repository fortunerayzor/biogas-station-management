﻿using BiogasStationManagement.Models.Storage;
using BiogasStationManagement.ViewModels.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BiogasStationManagement.ViewModels.Commands.Storage
{
    class DeleteTroughCommand : ICommand
    {
        private StorageManager storageManager;
        private PermissionManager permissionManager;

        public DeleteTroughCommand(StorageManager storageManager, PermissionManager permissionManager)
        {
            this.storageManager = storageManager;
            this.permissionManager = permissionManager;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            var trough = parameter as Trough;
            var usedByBatches = storageManager.Batches.Any(x => x.Ingredients.Any(y => y.TroughIndex == trough.TroughIndex));
            var usedByLoads = trough.Loads.Count > 0;
            return permissionManager.TroughsPermission.CanDelete && !usedByBatches && !usedByLoads;
        }

        public void Execute(object parameter)
        {
            var trough = parameter as Trough;
            storageManager.Troughs.Remove(trough);
        }
    }
}
