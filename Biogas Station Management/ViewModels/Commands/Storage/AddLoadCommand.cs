﻿using BiogasStationManagement.Models.Storage;
using BiogasStationManagement.ViewModels.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BiogasStationManagement.ViewModels.Commands.Storage
{
    class AddLoadCommand : ICommand
    {
        private StorageManager storageManager;
        private PermissionManager permissionManager;

        public AddLoadCommand(StorageManager storageManager, PermissionManager permissionManager)
        {
            this.storageManager = storageManager;
            this.permissionManager = permissionManager;
        }
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return permissionManager.TroughsPermission.CanModify && storageManager.Troughs.SelectedItem != null && !storageManager.Troughs.SelectedItem.IsLocked;
        }

        public void Execute(object parameter)
        {
            storageManager.Troughs.SelectedItem.Loads.Add(new Load() { Timestamp = DateTime.Now.Date });
        }
    }
}
