﻿using BiogasStationManagement.ViewModels.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BiogasStationManagement.ViewModels.Commands.Storage
{
    class InsertNewBatchCommand : ICommand
    {
        private StorageManager storageManager;
        private PermissionManager permissionManager;

        public InsertNewBatchCommand(StorageManager storageManager, PermissionManager permissionManager)
        {
            this.storageManager = storageManager;
            this.permissionManager = permissionManager;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return permissionManager.BatchesPermission.CanWrite && storageManager.NewBatch != null;
        }

        public void Execute(object parameter)
        {
            storageManager.Batches.Insert(0, storageManager.NewBatch);
            storageManager.NewBatch = null;
        }
    }
}
