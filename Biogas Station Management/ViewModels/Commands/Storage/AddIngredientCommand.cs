﻿using BiogasStationManagement.ViewModels.ModelVMs;
using BiogasStationManagement.ViewModels.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BiogasStationManagement.ViewModels.Commands.Storage
{
    class AddIngredientCommand : ICommand
    {
        private BatchVM batchVM;
        private PermissionManager permissionManager;

        public AddIngredientCommand(BatchVM batchVM, PermissionManager permissionManager)
        {
            this.batchVM = batchVM;
            this.permissionManager = permissionManager;
        }
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return permissionManager.BatchesPermission.CanModify && batchVM.Ingredients.All(x => x.Amount > 0d) && batchVM.Ingredients.Count < batchVM.TroughCount;
        }

        public void Execute(object parameter)
        {
            var allTroughs = batchVM.Ingredients.First().AllTroughs;
            if (allTroughs != null)
                batchVM.Ingredients.Add(new IngredientVM(allTroughs, batchVM, permissionManager));
        }
    }
}
