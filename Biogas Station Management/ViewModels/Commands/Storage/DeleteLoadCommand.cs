﻿using BiogasStationManagement.Models.Storage;
using BiogasStationManagement.ViewModels.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BiogasStationManagement.ViewModels.Commands.Storage
{
    class DeleteLoadCommand : ICommand
    {
        private StorageManager storageManager;
        private PermissionManager permissionManager;

        public DeleteLoadCommand(StorageManager storageManager, PermissionManager permissionManager)
        {
            this.storageManager = storageManager;
            this.permissionManager = permissionManager;
        }
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            var load = parameter as Load;
            if (load == null)
                return false;
            return permissionManager.TroughsPermission.CanModify;
        }

        public void Execute(object parameter)
        {
            storageManager.Troughs.SelectedItem.Loads.Remove((Load)parameter);
        }
    }
}
