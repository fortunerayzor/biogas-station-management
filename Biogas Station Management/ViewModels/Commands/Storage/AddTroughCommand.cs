﻿using BiogasStationManagement.ViewModels.Managers;
using BiogasStationManagement.ViewModels.ModelVMs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BiogasStationManagement.ViewModels.Commands.Storage
{
    class AddTroughCommand : ICommand
    {
        private StorageManager storageManager;
        private PermissionManager permissionManager;

        public AddTroughCommand(StorageManager storageManager, PermissionManager permissionManager)
        {
            this.storageManager = storageManager;
            this.permissionManager = permissionManager;
        }
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return permissionManager.TroughsPermission.CanWrite;
        }

        public void Execute(object parameter)
        {
            int troughIndex = 0;
            if (storageManager.Troughs.Count > 0)
                troughIndex = storageManager.Troughs.Max(x => x.TroughIndex) + 1;
            storageManager.Troughs.Add(new Models.Storage.Trough() { StorageName = "Nový žlab", TroughIndex = troughIndex });
        }
    }
}
