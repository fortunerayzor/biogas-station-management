﻿using BiogasStationManagement.ViewModels.Managers;
using BiogasStationManagement.ViewModels.ModelVMs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BiogasStationManagement.ViewModels.Commands.Storage
{
    class RemoveIngredientCommand : ICommand
    {
        private readonly BatchVM batchVM;
        private readonly IngredientVM ingredientVM;
        private readonly PermissionManager permissionManager;

        public RemoveIngredientCommand(BatchVM batchVM, IngredientVM ingredientVM, PermissionManager permissionManager)
        {
            this.batchVM = batchVM;
            this.ingredientVM = ingredientVM;
            this.permissionManager = permissionManager;
        }
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return permissionManager.BatchesPermission.CanModify;
        }

        public void Execute(object parameter)
        {
            batchVM.Ingredients.Remove(ingredientVM);
        }
    }
}