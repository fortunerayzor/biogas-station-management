﻿using BiogasStationManagement.Models.Storage;
using BiogasStationManagement.ViewModels.ModelVMs;
using BiogasStationManagement.ViewModels.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BiogasStationManagement.ViewModels.Commands.Storage
{
    class CreateNewBatchCommand : ICommand
    {
        private PermissionManager permissionManager;
        private StorageManager storageManager;
        public CreateNewBatchCommand(StorageManager storageManager, PermissionManager permissionManager)
        {
            this.storageManager = storageManager;
            this.permissionManager = permissionManager;

        }
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return permissionManager.BatchesPermission.CanWrite && storageManager.NewBatch == null && storageManager.Troughs.Where(x => x.IsLocked == false).Count() > 0;
        }

        public void Execute(object parameter)
        {
            storageManager.NewBatch = new BatchVM(DateTime.Now, storageManager.Troughs, permissionManager);
        }
    }
}
