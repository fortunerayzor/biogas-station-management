﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiogasStationManagement.ViewModels.Data
{
    internal class Permission : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public bool CanDelete { get; private set; }

        public bool CanModify { get; private set; }

        public bool CanRead { get; private set; }

        public bool CanWrite { get; private set; }

        internal void Reset()
        {
            CanRead = false;
            CanWrite = false;
            CanModify = false;
            CanDelete = false;
        }

        internal Permission SetPermissions(bool read, bool write, bool modify, bool delete)
        {
            CanRead = read;
            CanWrite = write;
            CanModify = modify;
            CanDelete = delete;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(string.Empty));
            return this;
        }
    }
}