﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using BiogasStationManagement.Models.Data;

namespace BiogasStationManagement.ViewModels.Data
{
    internal class DataSafeCollection<T> : SelectableCollection<T> where T : IDataIntegrity, INotifyPropertyChanged
    {
        private Permission permission;

        private bool revertingValuePermissionOverride = false;

        public DataSafeCollection(Permission permission)
        {
            this.permission = permission;
        }
        
        public event PropertyChangedEventHandler ItemPropertyChanged;

        public Permission UserPermission { get { return permission; } }

        protected override void ClearItems()
        {
            if (permission.CanDelete || !DataChangedSubscribed)
            {
                this.ToList().ForEach(x => x.PropertyChanged -= ItemPropertyChanged);
                base.ClearItems();
            }
            else
                MessageBox.Show("Kontaktujte správce aplikace. (chyba 10)", "Nepovolená akce", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Stop);
        }

        protected override void InsertItem(int index, T item)
        {
            if (permission.CanWrite || !DataChangedSubscribed)
            {
                base.InsertItem(index, item);
                item.PropertyChanged += Item_PropertyChanged;
            }
            else
            {
                MessageBox.Show("Kontaktujte správce aplikace. (chyba 11)", "Nepovolená akce", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Stop);
            }
        }

        protected override void OnDataChanged(DataChangedEventArgs e)
        {
            e.Index = IndexOf((T)e.SourceItem);
            bool permittedOperation = false;
            switch (e.Flag)
            {
                case DataChangedFlag.Added: permittedOperation = permission.CanWrite; break;
                case DataChangedFlag.Removed:
                case DataChangedFlag.Cleared: permittedOperation = permission.CanDelete; break;
                case DataChangedFlag.Modified:
                case DataChangedFlag.Moved:
                case DataChangedFlag.Unchanged:
                default: permittedOperation = permission.CanModify; break;
            }
            if (DataChangedSubscribed && permittedOperation)
            {
                base.OnDataChanged(e);
            }
            else if (revertingValuePermissionOverride)
            {
                revertingValuePermissionOverride = false;
                System.Diagnostics.Debug.WriteLine("Reverting value (item: {0})", e.SourceItem);
            }
            else if (!permittedOperation)
            {
                revertingValuePermissionOverride = true;
                RevertValue(e);
                MessageBox.Show("Kontaktujte správce aplikace. (chyba 15)", "Nepovolená akce", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Stop);
            }
            else if (DebugSubscription)
            {
                System.Diagnostics.Debug.WriteLine("Missing DataChanged subscription (item: {0})", e.SourceItem);
            }
        }

        private void RevertValue(DataChangedEventArgs e)
        {
            if (e.Flag == DataChangedFlag.Modified)
            {
                e.SourceItem.GetType().GetProperty(e.PropertyName, e.PropertyValueType).SetValue(e.SourceItem, e.PropertyOldValue);
                return;
            }
            else if (e.ChildEventArgs != null)
            {
                RevertValue(e.ChildEventArgs);
            }
        }

        protected override void MoveItem(int oldIndex, int newIndex)
        {
            if (permission.CanModify || !DataChangedSubscribed)
                base.MoveItem(oldIndex, newIndex);
            else
                MessageBox.Show("Kontaktujte správce aplikace. (chyba 12)", "Nepovolená akce", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Stop);
        }

        protected override void RemoveItem(int index)
        {
            if (permission.CanDelete || !DataChangedSubscribed)
            {
                this[index].PropertyChanged -= Item_PropertyChanged;
                base.RemoveItem(index);
            }
            else
            {
                MessageBox.Show("Kontaktujte správce aplikace. (chyba 13)", "Nepovolená akce", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Stop);
            }
        }

        protected override void SetItem(int index, T item)
        {
            if (permission.CanModify || !DataChangedSubscribed)
                base.SetItem(index, item);
            else
                MessageBox.Show("Kontaktujte správce aplikace. (chyba 14)", "Nepovolená akce", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Stop);
        }

        private void Item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            ItemPropertyChanged?.Invoke(sender, e);
        }
    }
}