﻿using BiogasStationManagement.Models;
using BiogasStationManagement.Models.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace BiogasStationManagement.ViewModels.Managers
{
    /// <summary>
    /// Class responsible for accepting commands from user interface.
    /// </summary>
    internal class AppliactionManager : INotifyPropertyChanged
    {
        private ContentManager contentManager;
        private Func<object, bool> navigationCallback;
        private string timeNow;
        private System.Timers.Timer timer = new System.Timers.Timer(1000);

        public AppliactionManager(Func<object, bool> navigationCallback)
        {
            this.navigationCallback = navigationCallback;
            // Command Initialization
            OpenPageCommand = new Commands.UI.OpenPageCommand(this);
            // Active clock
            TimeNow = DateTime.Now.ToLongTimeString();
            timer.AutoReset = true;
            timer.Elapsed += new System.Timers.ElapsedEventHandler((x, y) => TimeNow = DateTime.Now.ToLongTimeString());
            timer.Start();
            // manager init
            contentManager = new ContentManager();
            contentManager.OpenPageRequest += ContentManager_PageOpened;
            // open default (login) page
            OpenPage(new Views.Pages.Default());
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand OpenPageCommand
        {
            get;
            private set;
        }

        public string TimeNow
        {
            get { return timeNow; }
            private set
            {
                timeNow = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("TimeNow"));
            }
        }

        public void OpenPage(object page)
        {
            contentManager.OpenPage(page);
        }

        private void ContentManager_PageOpened(object sender, OpenPageRequestEventArgs e)
        {
            navigationCallback(e.RequestedPage);
        }
    }
}