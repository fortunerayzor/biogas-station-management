﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BiogasStationManagement.ViewModels.Data;

namespace BiogasStationManagement.ViewModels.Managers
{
    internal class PermissionManager
    {
        public PermissionManager()
        {
            var permissions = this.GetType().GetProperties().Where(x => x.PropertyType == typeof(Permission));
            permissions.ToList().ForEach(x => x.SetValue(this, new Permission()));
            SetPermissions(null);
        }

        public Permission BatchesPermission { get; private set; }
        public Permission EngineCheckHistoriesPermission { get; private set; }
        public Permission IrregularCheckHistoriesPermission { get; private set; }
        public Permission LogsPermission { get; private set; }
        public Permission PeriodicalCheckHistoriesPermission { get; private set; }
        public Permission TroughsPermission { get; private set; }
        public Permission UsersPermission { get; private set; }
        public Permission ExternalDataPermission { get; private set; }

        public void LoginManager_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "AuthenticatedUser")
            {
                SetPermissions(sender as LoginManager);
            }
        }

        private void SetPermissions(LoginManager loginManager)
        {
            if (loginManager == null)
            {
                UsersPermission.SetPermissions(true, false, false, false);
            }
            else if (loginManager.AuthenticatedUser == null)
            {
                var permissions = this.GetType().GetProperties().Where(x => x.PropertyType == typeof(Permission));
                foreach (var item in permissions)
                {
                    var permission = item.GetValue(this) as Permission;
                    permission?.Reset();
                }
            }
            else
            {
                var user = loginManager.AuthenticatedUser;
                BatchesPermission.SetPermissions(user.IsHandlerDriver, user.IsHandlerDriver, user.IsHandlerDriver, user.IsHandlerDriver);
                EngineCheckHistoriesPermission.SetPermissions(user.IsOperator, user.IsOperator, user.IsOperator, user.IsOperator);
                IrregularCheckHistoriesPermission.SetPermissions(user.IsOperator, user.IsOperator, user.IsOperator, user.IsOperator);
                LogsPermission.SetPermissions(user.IsOperator, user.IsOperator, user.IsOperator, user.IsOperator);
                PeriodicalCheckHistoriesPermission.SetPermissions(user.IsOperator, user.IsOperator, user.IsOperator, user.IsOperator);
                TroughsPermission.SetPermissions(user.IsHandlerDriver, user.IsHandlerDriver, user.IsHandlerDriver, user.IsHandlerDriver);
                UsersPermission.SetPermissions(true, user.IsSystemAdmin, user.IsSystemAdmin, user.IsSystemAdmin);
                ExternalDataPermission.SetPermissions(user.IsManager, user.IsManager, user.IsManager, user.IsManager);
            }
        }
    }
}