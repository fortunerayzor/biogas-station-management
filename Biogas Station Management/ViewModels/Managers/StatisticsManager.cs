﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using BiogasStationManagement.Models.Data;
using BiogasStationManagement.Models.Logs;
using BiogasStationManagement.Models.Statistics;
using BiogasStationManagement.ViewModels.Data;

namespace BiogasStationManagement.ViewModels.Managers
{
    internal class StatisticsManager : INotifyPropertyChanged
    {
        private readonly CultureInfo cultureInfo = CultureInfo.CreateSpecificCulture("cs-CZ");
        private readonly DataChangedHandler externalDataHandler;
        private DateTime formsTimePeriod;
        private DataSafeCollection<Models.Logs.LogEntity> logs;
        private Dictionary<string, double> vars = new Dictionary<string, double>();

        public StatisticsManager(IDataService dataService, ContentManager contentManager)
        {
            logs = contentManager.LoggingManager.Logs;
            logs.DataChanged += Logs_DataChanged;

            externalDataHandler = dataService.DataChangedHandler<ExternalData>;
            ExternalDataCollection = new DataSafeCollection<ExternalData>(contentManager.PermissionManager.ExternalDataPermission);
            ExternalDataCollection.SelectedItemChanged += ExternalDataCollection_SelectedItemChanged;
            ExternalDataCollection.ItemPropertyChanged += ExternalDataCollection_ItemPropertyChanged;
            

            if (!dataService.IsTypeRegistered(ExternalData.AssemblyQualifiedName))
                dataService.RegisterType(ExternalData.AssemblyQualifiedName);
            dataService.FetchItems(ExternalDataCollection, externalDataHandler);
            ExternalDataCollection.DebugSubscription = true;

            cultureInfo.NumberFormat.NumberGroupSeparator = string.Empty;
            FormsTimePeriod = DateTime.Now.GetYearMonth();
        }

        private void Logs_DataChanged(DataChangedEventArgs e)
        {
            RefreshFormData();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public DataSafeCollection<ExternalData> ExternalDataCollection { get; private set; }

        public DateTime FormsTimePeriod
        {
            get
            {
                return formsTimePeriod;
            }
            set
            {
                formsTimePeriod = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FormsTimePeriod"));
                SetExternalData();
            }
        }

        private void CalculateERU(LogEntity startLog, LogEntity endLog)
        {
            vars["TECH"] = ExternalDataCollection.SelectedItem.HeatConsumption;
            vars["HEATBRUT"] = vars["HEAT"] + vars["TECH"];
            vars["EXT"] = ExternalDataCollection.SelectedItem.HeatExternalSupply;
            vars["INT"] = ExternalDataCollection.SelectedItem.HeatInternalSupply;

            // section 3
            vars["E311"] = vars["GAS"] / 1000d;
            vars["E313"] = vars["HEATBRUT"] / (vars["HEATBRUT"] + vars["GCR2"] * 3.6d) * vars["E311"];
            vars["E312"] = vars["E311"] - vars["E313"];
            vars["E321"] = vars["LTO"] / 1000d;
            vars["E322"] = vars["E321"];
            // section 4
            vars["E411"] = vars["GCR2"] * (1d - vars["RES21"] / 100d);
            vars["E412"] = vars["GCR3"] * (1d - vars["RES21"] / 100d);
            vars["E417"] = vars["E411"] - vars["E412"];
            vars["E421"] = vars["HEATBRUT"];
            vars["E423"] = vars["TECH"];
            vars["E424"] = vars["INT"];
            vars["E425"] = vars["HEAT"] - vars["INT"] - vars["EXT"];
            vars["E427"] = vars["EXT"];
            vars["E431"] = vars["GCR2"] * (vars["RES21"] / 100d);
            vars["E432"] = vars["GCR3"] * (vars["RES21"] / 100d);
            vars["E437"] = vars["E431"] - vars["E432"];
            vars["E451"] = vars["GCR2"];
            vars["E452"] = vars["GCR3"];
            vars["E457"] = vars["GCR2"] - vars["GCR3"];
            vars["E461"] = vars["E421"]; //repeating section
            vars["E463"] = vars["E423"];
            vars["E464"] = vars["E424"];
            vars["E465"] = vars["E425"];
            vars["E467"] = vars["E427"];
            vars["E512"] = vars["GCR2"];
            vars["E513"] = vars["GCR3"];
            vars["E522"] = vars["E421"];
            vars["E523"] = vars["TECH"] + vars["INT"];
            vars["E526"] = vars["E425"];
            vars["E531"] = vars["GCR2"] - vars["GCR3"];
            vars["E711"] = vars["E411"];
            vars["E712"] = vars["TECH"] + vars["EXT"];
            vars["E713"] = vars["E311"];
            vars["E721"] = vars["E431"];
            vars["E723"] = vars["E321"];
            vars["E731"] = vars["E451"];
            vars["E732"] = vars["E712"];
        }

        private void CalculateOTE(LogEntity startLog, LogEntity endLog)
        {
            vars["CEZ"] = ExternalDataCollection.SelectedItem.ElectricityNettoDistribution; // MWh
            vars["METH"] = ExternalDataCollection.SelectedItem.MethanePercentage / 100d;
            vars["DENS"] = ExternalDataCollection.SelectedItem.OilDensity / 1000d; // kg/m^3
            vars["LTO"] = endLog.Engine1.OilCounter - startLog.Engine1.OilCounter +
                endLog.Engine2.OilCounter - startLog.Engine2.OilCounter +
                endLog.Engine3.OilCounter - startLog.Engine3.OilCounter; // kg
            vars["HEAT"] = endLog.Caliduct.EnergyCounter - startLog.Caliduct.EnergyCounter; // GJ
            vars["GAS"] = endLog.Engine1.GasCounter - startLog.Engine1.GasCounter +
                endLog.Engine2.GasCounter - startLog.Engine2.GasCounter +
                endLog.Engine3.GasCounter - startLog.Engine3.GasCounter; // m^3
            vars["GCR2"] = (endLog.Engine1.ElectricityCounter - startLog.Engine1.ElectricityCounter +
                endLog.Engine2.ElectricityCounter - startLog.Engine2.ElectricityCounter +
                endLog.Engine3.ElectricityCounter - startLog.Engine3.ElectricityCounter) / 1000d; // MWh
            vars["GCR3"] = vars["GCR2"] - vars["CEZ"];  // MWh
            vars["D17"] = (vars["LTO"] / vars["DENS"]) * 9.77d;
            vars["D18"] = vars["D17"] + vars["GAS"] * vars["METH"] * 9.96d;
            vars["D14"] = (vars["GCR2"] - vars["GCR3"]) * (vars["D17"] / vars["D18"]);
            vars["RES13_A"] = vars["CEZ"] - (vars["D14"]);
            vars["RES14_A"] = vars["HEAT"];
            vars["RES21"] = ((vars["D14"]) / vars["CEZ"]) * 100d;
        }

        private void ExternalDataCollection_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            RefreshFormData();
        }

        private void ExternalDataCollection_SelectedItemChanged()
        {
            RefreshFormData();
        }

        private bool LogDataExist()
        {
            LogEntity tmp1, tmp2;
            return LogDataExist(out tmp1, out tmp2);
        }

        private bool LogDataExist(out LogEntity startLog, out LogEntity endLog)
        {
            DateTime startDate = FormsTimePeriod.GetLastDateBefore(1);
            DateTime endDate = FormsTimePeriod.GetLastDateBefore(0);
            startLog = logs.Where(x => x.Timestamp.Date.Equals(startDate)).FirstOrDefault();
            endLog = logs.Where(x => x.Timestamp.Date.Equals(endDate)).FirstOrDefault();
            return startLog != null && endLog != null;
        }

        private void NotifyStringPropertiesChanged()
        {
            var properties = this.GetType().GetProperties().Where(x => x.PropertyType == typeof(string));
            foreach (var item in properties)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(item.Name));
            }
        }

        private void RefreshFormData()
        {
            if (ExternalDataCollection.SelectedItem == null ||
                ExternalDataCollection.SelectedItem.ElectricityNettoDistribution == 0d ||
                ExternalDataCollection.SelectedItem.HeatConsumption == 0d ||
                ExternalDataCollection.SelectedItem.HeatExternalSupply == 0d ||
                ExternalDataCollection.SelectedItem.HeatInternalSupply == 0d ||
                ExternalDataCollection.SelectedItem.MethanePercentage == 0d ||
                ExternalDataCollection.SelectedItem.OilDensity == 0d)
            {
                vars.Clear();
            }
            else
            {
                LogEntity startLog, endLog;
                if (!LogDataExist(out startLog, out endLog))
                {
                    vars.Clear();
                }
                else
                {
                    CalculateOTE(startLog, endLog);
                    CalculateERU(startLog, endLog);
                }
            }
            NotifyStringPropertiesChanged();
        }

        private void SetExternalData()
        {
            if (!LogDataExist())
            {
                if (ExternalDataCollection.SelectedItem != null)
                    ExternalDataCollection.SelectedItem = null;
                return;
            }

            DateTime yearMonth = FormsTimePeriod.GetYearMonth();
            var existingExternalData = ExternalDataCollection.Where(x => x.Timestamp.GetYearMonth() == yearMonth).FirstOrDefault();
            if (existingExternalData == null)
            {
                ExternalDataCollection.Add(new Models.Statistics.ExternalData() { Timestamp = yearMonth });
                ExternalDataCollection.SelectedItem = ExternalDataCollection.Last();
            }
            else
            {
                ExternalDataCollection.SelectedItem = existingExternalData;
            }
        }

        #region OTE Binding Properties

        public string OTE1
        {
            get { return vars.SafeGetValue("GCR2").ToString("N3", cultureInfo); }
        }

        public string OTE2
        {
            get { return vars.SafeGetValue("GCR3").ToString("N3", cultureInfo); }
        }

        public string OTE3
        {
            get { return vars.SafeGetValue("RES13_A").ToString("N3", cultureInfo); }
        }

        public string OTE4
        {
            get { return vars.SafeGetValue("RES14_A").ToString("N0", cultureInfo); }
        }

        public string OTE5
        {
            get { return vars.SafeGetValue("RES21").ToString("N3", cultureInfo); }
        }

        #endregion OTE Binding Properties

        #region ERU Binding Properties

        public string E311
        {
            get { return vars.SafeGetValue("E311").ToString("N3", cultureInfo); }
        }

        public string E312
        {
            get { return vars.SafeGetValue("E312").ToString("N3", cultureInfo); }
        }

        public string E313
        {
            get { return vars.SafeGetValue("E313").ToString("N3", cultureInfo); }
        }

        public string E321
        {
            get { return vars.SafeGetValue("E321").ToString("N3", cultureInfo); }
        }

        public string E322
        {
            get { return vars.SafeGetValue("E322").ToString("N3", cultureInfo); }
        }

        public string E411
        {
            get { return vars.SafeGetValue("E411").ToString("N3", cultureInfo); }
        }

        public string E412
        {
            get { return vars.SafeGetValue("E412").ToString("N3", cultureInfo); }
        }

        public string E417
        {
            get { return vars.SafeGetValue("E417").ToString("N3", cultureInfo); }
        }

        public string E421
        {
            get { return vars.SafeGetValue("E421").ToString("N3", cultureInfo); }
        }

        public string E423
        {
            get { return vars.SafeGetValue("E423").ToString("N3", cultureInfo); }
        }

        public string E424
        {
            get { return vars.SafeGetValue("E424").ToString("N3", cultureInfo); }
        }

        public string E425
        {
            get { return vars.SafeGetValue("E425").ToString("N3", cultureInfo); }
        }

        public string E427
        {
            get { return vars.SafeGetValue("E427").ToString("N3", cultureInfo); }
        }

        public string E431
        {
            get { return vars.SafeGetValue("E431").ToString("N3", cultureInfo); }
        }

        public string E432
        {
            get { return vars.SafeGetValue("E432").ToString("N3", cultureInfo); }
        }

        public string E437
        {
            get { return vars.SafeGetValue("E437").ToString("N3", cultureInfo); }
        }

        public string E451
        {
            get { return vars.SafeGetValue("E451").ToString("N3", cultureInfo); }
        }

        public string E452
        {
            get { return vars.SafeGetValue("E452").ToString("N3", cultureInfo); }
        }

        public string E457
        {
            get { return vars.SafeGetValue("E457").ToString("N3", cultureInfo); }
        }

        public string E461
        {
            get { return vars.SafeGetValue("E461").ToString("N3", cultureInfo); }
        }

        public string E463
        {
            get { return vars.SafeGetValue("E463").ToString("N3", cultureInfo); }
        }

        public string E464
        {
            get { return vars.SafeGetValue("E464").ToString("N3", cultureInfo); }
        }

        public string E465
        {
            get { return vars.SafeGetValue("E465").ToString("N3", cultureInfo); }
        }

        public string E467
        {
            get { return vars.SafeGetValue("E467").ToString("N3", cultureInfo); }
        }

        public string E512
        {
            get { return vars.SafeGetValue("E512").ToString("N3", cultureInfo); }
        }

        public string E513
        {
            get { return vars.SafeGetValue("E513").ToString("N3", cultureInfo); }
        }

        public string E522
        {
            get { return vars.SafeGetValue("E522").ToString("N3", cultureInfo); }
        }

        public string E523
        {
            get { return vars.SafeGetValue("E523").ToString("N3", cultureInfo); }
        }

        public string E526
        {
            get { return vars.SafeGetValue("E526").ToString("N3", cultureInfo); }
        }

        public string E531
        {
            get { return vars.SafeGetValue("E531").ToString("N3", cultureInfo); }
        }

        public string E543
        {
            get
            {
                return vars.SafeGetValue("E543").ToString("N3", cultureInfo);
            }
            set
            {
                double var;
                if (double.TryParse(value, out var))
                {
                    vars["E543"] = var;
                    vars["E545"] = vars["EXT"] - vars.SafeGetValue("E544") - vars["E543"];
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("E543"));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("E545"));
                }
            }
        }

        public string E544
        {
            get
            {
                return vars.SafeGetValue("E544").ToString("N3", cultureInfo);
            }
            set
            {
                double var;
                if (double.TryParse(value, out var))
                {
                    vars["E544"] = var;
                    vars["E545"] = vars["EXT"] - vars["E544"] - vars.SafeGetValue("E543");
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("E544"));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("E545"));
                }
            }
        }

        public string E545
        {
            get { return vars.SafeGetValue("E545").ToString("N3", cultureInfo); }
        }

        public string E711
        {
            get { return vars.SafeGetValue("E711").ToString("N3", cultureInfo); }
        }

        public string E712
        {
            get { return vars.SafeGetValue("E712").ToString("N3", cultureInfo); }
        }

        public string E713
        {
            get { return vars.SafeGetValue("E713").ToString("N3", cultureInfo); }
        }

        public string E721
        {
            get { return vars.SafeGetValue("E721").ToString("N3", cultureInfo); }
        }

        public string E723
        {
            get { return vars.SafeGetValue("E723").ToString("N3", cultureInfo); }
        }

        public string E731
        {
            get { return vars.SafeGetValue("E731").ToString("N3", cultureInfo); }
        }

        public string E732
        {
            get { return vars.SafeGetValue("E732").ToString("N3", cultureInfo); }
        }

        #endregion ERU Binding Properties
    }
}