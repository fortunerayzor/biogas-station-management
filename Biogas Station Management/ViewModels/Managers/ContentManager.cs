﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using BiogasStationManagement.Models.Data;

namespace BiogasStationManagement.ViewModels.Managers
{
    public delegate void OpenPageRequestHandler(object sender, OpenPageRequestEventArgs e);

    public class OpenPageRequestEventArgs
    {
        public OpenPageRequestEventArgs(Page openedPage)
        {
            RequestedPage = openedPage;
        }

        public Page RequestedPage { get; private set; }
    }

    /// <summary>
    /// Class responsible for providing content to opened pages and user permissions.
    /// </summary>
    internal class ContentManager : INotifyPropertyChanged
    {
        private IDataService dataService;

        public ContentManager()
        {
            // Data service initialization
            dataService = new LocalDataService();
            // Manager initialization
            PermissionManager = new PermissionManager();
            LoginManager = new LoginManager(dataService, PermissionManager);
            LoggingManager = new LoggingManager(dataService, PermissionManager);
            StorageManager = new StorageManager(dataService, PermissionManager);
            CheckManager = new CheckManager(dataService, PermissionManager, LoggingManager);
            StatisticsManager = new StatisticsManager(dataService, this);
            PropertyChanged += CheckManager.OnWarningLimitsChanged;
        }

        public event OpenPageRequestHandler OpenPageRequest;
        public event PropertyChangedEventHandler PropertyChanged;

        public CheckManager CheckManager { get; private set; }

        public LoggingManager LoggingManager { get; private set; }

        public LoginManager LoginManager { get; private set; }

        public PermissionManager PermissionManager { get; private set; }

        public StorageManager StorageManager { get; private set; }

        public StatisticsManager StatisticsManager { get; private set; }

        public double ServiceIntervalWarning
        {
            get
            {
                return Properties.Settings.Default.ServiceIntervalWarning;
            }
            set
            {
                Properties.Settings.Default.ServiceIntervalWarning = value;
                Properties.Settings.Default.Save();
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ServiceIntervalWarning"));
            }
        }

        public double PeriodicalCheckWarning
        {
            get
            {
                return Properties.Settings.Default.PeriodicalCheckWarning.TotalDays;
            }
            set
            {
                Properties.Settings.Default.PeriodicalCheckWarning = TimeSpan.FromDays(value);
                Properties.Settings.Default.Save();
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("PeriodicalCheckWarning"));
            }
        }

        public void OpenPage(object page, object dataContext)
        {
            Page p = page as Page;
            if (p != null)
            {
                p.DataContext = dataContext;
                OpenPageRequest?.Invoke(this, new OpenPageRequestEventArgs(p));
            }
        }

        public void OpenPage(object page)
        {
            OpenPage(page, this);
        }
    }
}