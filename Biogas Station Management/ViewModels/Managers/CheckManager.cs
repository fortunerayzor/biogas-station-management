﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using BiogasStationManagement.Models.Checks;
using BiogasStationManagement.Models.Data;
using BiogasStationManagement.Models.Logs;
using BiogasStationManagement.ViewModels.Commands.Checks.Engine;
using BiogasStationManagement.ViewModels.Commands.Checks.Irregular;
using BiogasStationManagement.ViewModels.Commands.Checks.Periodical;
using BiogasStationManagement.ViewModels.Data;
using BiogasStationManagement.ViewModels.ModelVMs;

namespace BiogasStationManagement.ViewModels.Managers
{
    internal class CheckManager : INotifyPropertyChanged
    {
        private CollectionViewSource engineCheckHistoriesViewSource;
        private DataChangedHandler engineChecksHandler;
        private CollectionViewSource irregularCheckHistoriesViewSource;
        private DataChangedHandler irregularChecksHandler;
        private LoggingManager loggingManager;
        private EngineCheckHistoryVM newEngineCheckHistory;
        private IrregularCheckHistoryVM newIrregularCheckHistory;
        private PeriodicalCheckHistoryVM newPeriodicalCheckHistory;
        private CollectionViewSource periodicalCheckHistoriesViewSource;
        private DataChangedHandler periodicalChecksHandler;
        private CollectionViewSource scheduledEngineChecksViewSource;

        private CollectionViewSource scheduledPeriodicalChecksViewSource;

        public CheckManager(IDataService dataService, PermissionManager permissionManager, LoggingManager loggingManager)
        {
            engineChecksHandler = dataService.DataChangedHandler<EngineCheckHistory>;
            periodicalChecksHandler = dataService.DataChangedHandler<PeriodicalCheckHistory>;
            irregularChecksHandler = dataService.DataChangedHandler<IrregularCheckHistory>;

            EngineCheckHistories = new DataSafeCollection<EngineCheckHistoryVM>(permissionManager.EngineCheckHistoriesPermission);
            EngineCheckHistories.DataChanged += engineChecksHandler;
            EngineCheckHistories.ItemPropertyChanged += EngineCheckHistories_ItemPropertyChanged;
            PeriodicalCheckHistories = new DataSafeCollection<PeriodicalCheckHistoryVM>(permissionManager.PeriodicalCheckHistoriesPermission);
            PeriodicalCheckHistories.DataChanged += periodicalChecksHandler;
            IrregularCheckHistories = new DataSafeCollection<IrregularCheckHistoryVM>(permissionManager.IrregularCheckHistoriesPermission);
            IrregularCheckHistories.DataChanged += irregularChecksHandler;

            this.loggingManager = loggingManager;
            loggingManager.Logs.DataChanged += Logs_DataChanged;

            CreateNewEngineCheckHistoryCommand = new CreateNewEngineCheckHistoryCommand(this, permissionManager);
            InsertNewEngineCheckHistoryCommand = new InsertNewEngineCheckHistoryCommand(this, permissionManager);
            DeleteNewEngineCheckHistoryCommand = new DeleteNewEngineCheckHistoryCommand(this, permissionManager);

            CreateNewPeriodicalCheckHistoryCommand = new CreateNewPeriodicalCheckHistoryCommand(this, permissionManager);
            InsertNewPeriodicalCheckHistoryCommand = new InsertNewPeriodicalCheckHistoryCommand(this, permissionManager);
            DeleteNewPeriodicalCheckHistoryCommand = new DeleteNewPeriodicalCheckHistoryCommand(this, permissionManager);

            CreateNewIrregularCheckHistoryCommand = new CreateNewIrregularCheckHistoryCommand(this, permissionManager);
            InsertNewIrregularCheckHistoryCommand = new InsertNewIrregularCheckHistoryCommand(this, permissionManager);
            DeleteNewIrregularCheckHistoryCommand = new DeleteNewIrregularCheckHistoryCommand(this, permissionManager);

            if (!dataService.IsTypeRegistered(EngineCheckHistory.AssemblyQualifiedName))
                dataService.RegisterType(EngineCheckHistory.AssemblyQualifiedName);
            if (!dataService.IsTypeRegistered(PeriodicalCheckHistory.AssemblyQualifiedName))
                dataService.RegisterType(PeriodicalCheckHistory.AssemblyQualifiedName);
            if (!dataService.IsTypeRegistered(IrregularCheckHistory.AssemblyQualifiedName))
                dataService.RegisterType(IrregularCheckHistory.AssemblyQualifiedName);

            FetchEngineCheckHistories(dataService, permissionManager);
            FetchPeriodicalCheckHistories(dataService, permissionManager);
            FetchIrregularCheckHistories(dataService, permissionManager);

            // Create Live views
            engineCheckHistoriesViewSource = new CollectionViewSource { Source = EngineCheckHistories };
            EngineCheckHistoriesView = engineCheckHistoriesViewSource.View;
            EngineCheckHistoriesView.EnableLiveSorting("RemainingHours", ListSortDirection.Ascending);
            RefreshEngineRemainingHours();

            periodicalCheckHistoriesViewSource = new CollectionViewSource { Source = PeriodicalCheckHistories };
            PeriodicalCheckHistoriesView = periodicalCheckHistoriesViewSource.View;
            PeriodicalCheckHistoriesView.EnableLiveSorting("RemainingTime", ListSortDirection.Ascending);

            irregularCheckHistoriesViewSource = new CollectionViewSource { Source = IrregularCheckHistories };
            IrregularCheckHistoriesView = irregularCheckHistoriesViewSource.View;
            IrregularCheckHistoriesView.EnableLiveSorting("Name", ListSortDirection.Ascending);

            scheduledEngineChecksViewSource = new CollectionViewSource { Source = EngineCheckHistories };
            ScheduledEngineChecksView = scheduledEngineChecksViewSource.View;
            ScheduledEngineChecksView.EnableLiveFiltering(IsEngineCheckSoon, "RemainingHours");
            ScheduledEngineChecksView.EnableLiveSorting("RemainingHours", ListSortDirection.Ascending);

            scheduledPeriodicalChecksViewSource = new CollectionViewSource { Source = PeriodicalCheckHistories };
            ScheduledPeriodicalChecksView = scheduledPeriodicalChecksViewSource.View;
            ScheduledPeriodicalChecksView.EnableLiveFiltering(IsPeriodicalCheckSoon, "RemainingTime");
            ScheduledPeriodicalChecksView.EnableLiveSorting("RemainingTime", ListSortDirection.Ascending);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public EngineCheckHistoryVM NewEngineCheckHistory
        {
            get
            {
                return newEngineCheckHistory;
            }
            set
            {
                newEngineCheckHistory = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("NewEngineCheckHistory"));
            }
        }

        public IrregularCheckHistoryVM NewIrregularCheckHistory
        {
            get
            {
                return newIrregularCheckHistory;
            }
            set
            {
                newIrregularCheckHistory = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("NewIrregularCheckHistory"));
            }
        }

        public PeriodicalCheckHistoryVM NewPeriodicalCheckHistory
        {
            get
            {
                return newPeriodicalCheckHistory;
            }
            set
            {
                newPeriodicalCheckHistory = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("NewPeriodicalCheckHistory"));
            }
        }

        public void RefreshEngineRemainingHours(EngineCheckHistoryVM engineCheckHistoryVM = null)
        {
            LogEntity latestLog = null;
            if (loggingManager.Logs.Count > 0)
                latestLog = loggingManager.LogsView.Cast<LogEntity>()?.First();
            if (engineCheckHistoryVM != null)
            {
                RefreshEngineRemainingHours(latestLog, engineCheckHistoryVM);
            }
            else
            {
                foreach (var item in EngineCheckHistories)
                {
                    RefreshEngineRemainingHours(latestLog, item);
                }
            }
        }

        internal void OnWarningLimitsChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ServiceIntervalWarning")
                RefreshEngineRemainingHours();
            if (e.PropertyName == "PeriodicalCheckWarning")
                ScheduledPeriodicalChecksView.Refresh();
        }

        #region Commands

        public ICommand CreateNewEngineCheckHistoryCommand { get; private set; }
        public ICommand CreateNewIrregularCheckHistoryCommand { get; private set; }
        public ICommand CreateNewPeriodicalCheckHistoryCommand { get; private set; }
        public ICommand DeleteNewEngineCheckHistoryCommand { get; private set; }
        public ICommand DeleteNewIrregularCheckHistoryCommand { get; private set; }
        public ICommand DeleteNewPeriodicalCheckHistoryCommand { get; private set; }
        public ICommand InsertNewEngineCheckHistoryCommand { get; private set; }
        public ICommand InsertNewIrregularCheckHistoryCommand { get; private set; }
        public ICommand InsertNewPeriodicalCheckHistoryCommand { get; private set; }

        #endregion Commands

        #region CollectionViews

        public ICollectionView EngineCheckHistoriesView { get; private set; }

        public ICollectionView IrregularCheckHistoriesView { get; private set; }

        public ICollectionView PeriodicalCheckHistoriesView { get; private set; }

        public ICollectionView ScheduledEngineChecksView { get; private set; }

        public ICollectionView ScheduledPeriodicalChecksView { get; private set; }

        #endregion CollectionViews

        #region Collections

        public DataSafeCollection<EngineCheckHistoryVM> EngineCheckHistories { get; private set; }

        public DataSafeCollection<IrregularCheckHistoryVM> IrregularCheckHistories { get; private set; }

        public DataSafeCollection<PeriodicalCheckHistoryVM> PeriodicalCheckHistories { get; private set; }

        #endregion Collections

        private void EngineCheckHistories_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ServiceInterval" || e.PropertyName == "AddIntervalFromLastCheck" || e.PropertyName == "DataChangedBeacon")
            {
                RefreshEngineRemainingHours(sender as EngineCheckHistoryVM);
            }
        }

        private bool EngineNeedsCheckWarning(double currentEngineHours, EngineCheckHistoryVM history)
        {
            var settings = Properties.Settings.Default;
            return history.RemainingHours < settings.ServiceIntervalWarning;
        }

        private void FetchEngineCheckHistories(IDataService dataService, PermissionManager permissionManager)
        {
            SelectableCollection<EngineCheckHistory> loadedHistories = new SelectableCollection<EngineCheckHistory>();
            dataService.FetchItems<EngineCheckHistory>(loadedHistories, null);
            EngineCheckHistories.DataChanged -= engineChecksHandler;
            if (EngineCheckHistories.Count > 0)
                EngineCheckHistories.Clear();
            foreach (var item in loadedHistories)
            {
                EngineCheckHistories.Add(new EngineCheckHistoryVM(this, item, permissionManager));
            }
            loadedHistories.Clear();
            EngineCheckHistories.DataChanged += engineChecksHandler;
            EngineCheckHistories.DebugSubscription = true;
        }

        private void FetchIrregularCheckHistories(IDataService dataService, PermissionManager permissionManager)
        {
            SelectableCollection<IrregularCheckHistory> loadedHistories = new SelectableCollection<IrregularCheckHistory>();
            dataService.FetchItems<IrregularCheckHistory>(loadedHistories, null);
            IrregularCheckHistories.DataChanged -= irregularChecksHandler;
            if (IrregularCheckHistories.Count > 0)
                IrregularCheckHistories.Clear();
            foreach (var item in loadedHistories)
            {
                IrregularCheckHistories.Add(new IrregularCheckHistoryVM(this, item, permissionManager));
            }
            loadedHistories.Clear();
            IrregularCheckHistories.DataChanged += irregularChecksHandler;
            IrregularCheckHistories.DebugSubscription = true;
        }

        private void FetchPeriodicalCheckHistories(IDataService dataService, PermissionManager permissionManager)
        {
            SelectableCollection<PeriodicalCheckHistory> loadedHistories = new SelectableCollection<PeriodicalCheckHistory>();
            dataService.FetchItems<PeriodicalCheckHistory>(loadedHistories, null);
            PeriodicalCheckHistories.DataChanged -= periodicalChecksHandler;
            if (PeriodicalCheckHistories.Count > 0)
                PeriodicalCheckHistories.Clear();
            foreach (var item in loadedHistories)
            {
                PeriodicalCheckHistories.Add(new PeriodicalCheckHistoryVM(this, item, permissionManager));
            }
            loadedHistories.Clear();
            PeriodicalCheckHistories.DataChanged += periodicalChecksHandler;
            PeriodicalCheckHistories.DebugSubscription = true;
        }

        private bool IsEngineCheckSoon(object obj)
        {
            if (loggingManager.Logs.Count == 0)
                return false;
            var history = obj as EngineCheckHistoryVM;
            switch (history?.EngineIndex)
            {
                case 1:
                    var kj1 = loggingManager.LogsView.Cast<LogEntity>()?.First().Engine1.RunningHoursCounter;
                    if (kj1.HasValue)
                        return EngineNeedsCheckWarning(kj1.Value, history);
                    return false;

                case 2:
                    var kj2 = loggingManager.LogsView.Cast<LogEntity>()?.First().Engine2.RunningHoursCounter;
                    if (kj2.HasValue)
                        return EngineNeedsCheckWarning(kj2.Value, history);
                    return false;

                case 3:
                    var kj3 = loggingManager.LogsView.Cast<LogEntity>()?.First().Engine3.RunningHoursCounter;
                    if (kj3.HasValue)
                        return EngineNeedsCheckWarning(kj3.Value, history);
                    return false;

                default:
                    return false;
            }
        }

        private bool IsPeriodicalCheckSoon(object obj)
        {
            var settings = Properties.Settings.Default;
            var history = obj as PeriodicalCheckHistoryVM;
            if (history == null)
                return false;
            return history.RemainingTime - TimeSpan.FromDays(1) <= settings.PeriodicalCheckWarning; // check must be completed before expiration
        }

        private void Logs_DataChanged(DataChangedEventArgs e)
        {
            LogEntity latestLog = null;
            if (loggingManager.Logs.Count > 0)
                latestLog = loggingManager.LogsView.Cast<LogEntity>()?.First();
            if ((latestLog != null && e.SourceItem == latestLog) || e.Flag == DataChangedFlag.Added)
                RefreshEngineRemainingHours();
        }

        private void RefreshEngineRemainingHours(LogEntity latestLog, EngineCheckHistoryVM engineCheckHistoryVM)
        {
            switch (engineCheckHistoryVM.EngineIndex)
            {
                case 1: engineCheckHistoryVM.RefreshRemainingHours(latestLog.Engine1.RunningHoursCounter); break;
                case 2: engineCheckHistoryVM.RefreshRemainingHours(latestLog.Engine2.RunningHoursCounter); break;
                case 3: engineCheckHistoryVM.RefreshRemainingHours(latestLog.Engine3.RunningHoursCounter); break;
            }
        }
    }
}