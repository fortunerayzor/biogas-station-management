﻿using BiogasStationManagement.Models;
using BiogasStationManagement.Models.Data;
using BiogasStationManagement.ViewModels.Commands.Login;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Input;
using System.Xml.Serialization;
using BiogasStationManagement.ViewModels.Data;

namespace BiogasStationManagement.ViewModels.Managers
{
    internal class LoginManager : INotifyPropertyChanged
    {
        private User authenticatedUser;
        private string loginName = string.Empty, loginPassword = string.Empty;
        private DataSafeCollection<User> users;
        private DataChangedHandler usersHandler;

        public LoginManager(IDataService dataService, PermissionManager permissionManager)
        {
            // Field initialization
            usersHandler = dataService.DataChangedHandler<User>;
            users = new DataSafeCollection<User>(permissionManager.UsersPermission);
            users.DataChanged += usersHandler;
            // Commands initialization
            // TODO: use permissionmanager in login commands
            LoginCommand = new LoginCommand(this);
            LogoffCommand = new LogoffCommand(this);
            RegisterAdminCommand = new RegisterAdminCommand(this);
            AddNewUserCommand = new AddNewUserCommand(this);
            DeleteSelectedUserCommand = new DeleteSelectedUserCommand(this);
            SetNewUserPasswordCommand = new SetNewUserPasswordCommand(this);
            // Data service registration for User class
            if (!dataService.IsTypeRegistered(User.AssemblyQualifiedName))
                dataService.RegisterType(User.AssemblyQualifiedName);
            // Loading users
            dataService.FetchItems<User>(Users, usersHandler);
            Users.DebugSubscription = true;
            // Hooking permission manager to change permissions accordingly
            PropertyChanged += permissionManager.LoginManager_PropertyChanged;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand AddNewUserCommand { get; private set; }

        public User AuthenticatedUser
        {
            get { return authenticatedUser; }
            set
            {
                authenticatedUser = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AuthenticatedUser"));
            }
        }

        public ICommand DeleteSelectedUserCommand { get; private set; }

        public ICommand LoginCommand { get; private set; }

        public string LoginName
        {
            get { return loginName; }
            set
            {
                loginName = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("LoginName"));
            }
        }

        public string LoginPassword
        {
            get { return loginPassword; }
            set
            {
                loginPassword = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("LoginPassword"));
            }
        }

        public ICommand LogoffCommand { get; private set; }

        public bool NoUsers
        {
            get { return users.Count == 0; }
        }

        public ICommand RegisterAdminCommand { get; private set; }

        public ICommand SetNewUserPasswordCommand { get; private set; }

        /// <summary>
        /// Gets the list of all users.
        /// </summary>
        public DataSafeCollection<User> Users
        {
            get { return users; }
            private set { users = value; }
        }

        public bool ValidLoginCredentials
        {
            get
            {
                return LoginName != string.Empty && LoginPassword != string.Empty;
            }
        }

        public void AuthenticateUser()
        {
            User authenticatingUser = users.FirstOrDefault(x => x.Name == LoginName);
            if (authenticatingUser != null && LoginPassword.VerifyPassword(authenticatingUser.PasswordHash))
            {
                AuthenticatedUser = authenticatingUser;
                LoginName = string.Empty;
                LoginPassword = string.Empty;
            }
            else
            {
                LoginPassword = string.Empty;
            }
        }

        public void LogoffUser()
        {
            AuthenticatedUser = null;
        }

        public void RegisterFirstUser()
        {
            User firstUser = new User()
            {
                Name = LoginName,
                PasswordHash = LoginPassword.GeneratePasswordHash(),
                IsSystemAdmin = true
            };
            LoginName = string.Empty;
            LoginPassword = string.Empty;
            AuthenticatedUser = firstUser;
            users.Add(firstUser);
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("NoUsers"));
        }

        public bool SameUserNameExists()
        {
            return users.Where(x => x.Name == LoginName).Count() > 0;
        }
    }
}