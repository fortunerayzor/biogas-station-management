﻿using BiogasStationManagement.Models.Data;
using BiogasStationManagement.Models.Storage;
using BiogasStationManagement.ViewModels.Commands.Storage;
using BiogasStationManagement.ViewModels.ModelVMs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using BiogasStationManagement.ViewModels.Data;

namespace BiogasStationManagement.ViewModels.Managers
{
    internal class StorageManager : INotifyPropertyChanged
    {
        private DataChangedHandler batchesHandler;
        private BatchVM newBatch;
        private DataChangedHandler troughsHandler;

        public StorageManager(IDataService dataService, PermissionManager permissionManager)
        {
            batchesHandler = dataService.DataChangedHandler<Batch>;
            troughsHandler = dataService.DataChangedHandler<Trough>;

            CreateNewBatchCommand = new CreateNewBatchCommand(this, permissionManager);
            InsertNewBatchCommand = new InsertNewBatchCommand(this, permissionManager);
            DeleteNewBatchCommand = new DeleteNewBatchCommand(this, permissionManager);
            DeleteBatchCommand = new DeleteBatchCommand(this, permissionManager);
            AddTroughCommand = new AddTroughCommand(this, permissionManager);
            DeleteTroughCommand = new DeleteTroughCommand(this, permissionManager);
            AddLoadCommand = new AddLoadCommand(this, permissionManager);
            DeleteLoadCommand = new DeleteLoadCommand(this, permissionManager);

            Batches = new DataSafeCollection<BatchVM>(permissionManager.BatchesPermission);
            Batches.DataChanged += batchesHandler;
            Troughs = new DataSafeCollection<Trough>(permissionManager.TroughsPermission);
            Troughs.DataChanged += troughsHandler;

            if (!dataService.IsTypeRegistered(Batch.AssemblyQualifiedName))
                dataService.RegisterType(Batch.AssemblyQualifiedName);
            if (!dataService.IsTypeRegistered(Trough.AssemblyQualifiedName))
                dataService.RegisterType(Trough.AssemblyQualifiedName);

            dataService.FetchItems<Trough>(Troughs, troughsHandler);
            FetchBatches(dataService, permissionManager);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand AddLoadCommand
        {
            get; private set;
        }

        public ICommand AddTroughCommand
        {
            get; private set;
        }

        public DataSafeCollection<BatchVM> Batches { get; private set; }

        public ICommand CreateNewBatchCommand
        {
            get; private set;
        }

        public ICommand DeleteBatchCommand
        {
            get; private set;
        }

        public ICommand DeleteLoadCommand
        {
            get; private set;
        }

        public ICommand DeleteNewBatchCommand
        {
            get; private set;
        }

        public ICommand DeleteTroughCommand
        {
            get; private set;
        }

        public ICommand InsertNewBatchCommand
        {
            get; private set;
        }

        public BatchVM NewBatch
        {
            get { return newBatch; }
            set
            {
                newBatch = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("NewBatch"));
            }
        }

        public DataSafeCollection<Trough> Troughs { get; private set; }

        // workaround for using BatchVM wrapping class
        private void FetchBatches(IDataService dataService, PermissionManager permissionManager)
        {
            SelectableCollection<Batch> loadedBatches = new SelectableCollection<Batch>();
            dataService.FetchItems<Batch>(loadedBatches, null);
            Batches.DataChanged -= batchesHandler;
            if (Batches.Count > 0)
                Batches.Clear();
            foreach (var item in loadedBatches)
            {
                Batches.Add(new BatchVM(item, Troughs, permissionManager));
            }
            loadedBatches.Clear();
            Batches.DataChanged += batchesHandler;
            Batches.DebugSubscription = true;
        }
    }
}