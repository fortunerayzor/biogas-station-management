﻿using BiogasStationManagement.Models.Data;
using BiogasStationManagement.Models.Logs;
using BiogasStationManagement.ViewModels.Commands.DailyLogging;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Data;
using System.Windows.Input;
using BiogasStationManagement.ViewModels.Data;

namespace BiogasStationManagement.ViewModels.Managers
{
    internal class LoggingManager : INotifyPropertyChanged
    {
        private LogEntity newLog;
        private DataChangedHandler logsHandler;

        public LoggingManager(IDataService dataService, PermissionManager permissionManager)
        {
            // Fields initialization section
            logsHandler = dataService.DataChangedHandler<LogEntity>;
            Logs = new DataSafeCollection<LogEntity>(permissionManager.LogsPermission);
            Logs.DataChanged += logsHandler;
            // Commands initilization
            CreateNewLogCommand = new CreateNewLogCommand(this, permissionManager);
            InsertNewLogCommand = new InsertNewLogCommand(this, permissionManager);
            DeleteNewLogCommand = new DeleteNewLogCommand(this, permissionManager);
            DeleteLogCommand = new DeleteLogCommand(this, permissionManager);
            // Class registration section
            if (!dataService.IsTypeRegistered(LogEntity.AssemblyQualifiedName))
                dataService.RegisterType(LogEntity.AssemblyQualifiedName); // Register Log type if not registered already
            dataService.FetchItems<LogEntity>(Logs, logsHandler);
            Logs.DebugSubscription = true;
            // Live view initialization
            LogsView = CollectionViewSource.GetDefaultView(Logs);
            LogsView.EnableLiveSorting("Timestamp", ListSortDirection.Descending);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand CreateNewLogCommand { get; private set; }

        public ICommand DeleteLogCommand { get; private set; }

        public ICommand DeleteNewLogCommand { get; private set; }
        
        public ICommand InsertNewLogCommand { get; private set; }

        public DataSafeCollection<LogEntity> Logs { get; private set; }

        public ICollectionView LogsView { get; private set; }

        public LogEntity NewLog
        {
            get { return newLog; }
            set
            {
                newLog = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("NewLog"));
            }
        }
    }
}