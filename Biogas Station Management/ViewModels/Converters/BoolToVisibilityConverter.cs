﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BiogasStationManagement.ViewModels.Converters
{
    class BoolToVisibilityConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value.GetType().Equals(typeof(bool)) && (bool)value == true)
                return Visibility.Visible;
            else
                return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (targetType.Equals(typeof(Visibility)))
            {
                switch ((Visibility)value)
                {
                    case Visibility.Collapsed:
                        return false;

                    case Visibility.Hidden:
                        return false;

                    case Visibility.Visible:
                        return true;

                    default:
                        return false;
                }
            }
            return false;
        }
    }
}
