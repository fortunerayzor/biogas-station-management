﻿using System;
using System.Windows;

namespace BiogasStationManagement.ViewModels.Converters
{
    public class ObjectToVisibilityConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return Visibility.Collapsed;
            else
                return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            switch ((Visibility)value)
            {
                case Visibility.Collapsed:
                    return null;

                case Visibility.Hidden:
                    return null;

                case Visibility.Visible:
                    return Activator.CreateInstance(targetType);

                default:
                    return null;
            }
        }
    }
}