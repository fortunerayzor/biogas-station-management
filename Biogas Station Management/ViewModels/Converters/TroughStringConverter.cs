﻿using BiogasStationManagement.Models.Storage;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiogasStationManagement.ViewModels.Converters
{
    class TroughStringConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var trough = value as Trough;
            if (trough == null)
                return string.Empty;
            return String.Format("{0}, {1}", trough.StorageName, trough.Description);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
