﻿using BiogasStationManagement.Models.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace BiogasStationManagement.ViewModels
{
    internal static class Helper
    {
        private static int derivedKeyLength = 24;
        private static int saltByteLength = 24;
        private static int iterationCount = 1000;
        public static byte[] GenerateHashValue(string password, byte[] salt, int iterationCount)
        {
            if (password == null || password == string.Empty)
            {
                throw new ArgumentException("Password must not be empty.", "password");
            }
            byte[] hashValue;
            using (Rfc2898DeriveBytes pbkdf2 = new Rfc2898DeriveBytes(password, salt, iterationCount))
            {
                hashValue = pbkdf2.GetBytes(derivedKeyLength);
            }
            return hashValue;
        }

        public static string GeneratePasswordHash(this string password)
        {
            if (password == null || password == string.Empty)
            {
                return string.Empty;
            }
            byte[] iterationCountArray = BitConverter.GetBytes(iterationCount);
            byte[] passwordHash = new byte[derivedKeyLength + saltByteLength + iterationCountArray.Length];
            byte[] salt = GenerateRandomSalt();
            byte[] hashValue = GenerateHashValue(password, salt, iterationCount);
            Buffer.BlockCopy(hashValue, 0, passwordHash, 0, derivedKeyLength);
            Buffer.BlockCopy(salt, 0, passwordHash, derivedKeyLength, saltByteLength);
            Buffer.BlockCopy(iterationCountArray, 0, passwordHash, derivedKeyLength + saltByteLength, iterationCountArray.Length);
            return Convert.ToBase64String(passwordHash);
        }

        public static bool VerifyPassword(this string password, string hash)
        {
            if (password == null || password == string.Empty)
            {
                return false;
            }
            byte[] actualPasswordHash = Convert.FromBase64String(hash);
            byte[] actualDerivedKey = new byte[derivedKeyLength];
            byte[] salt = new byte[saltByteLength];
            byte[] iterationCountArray = new byte[actualPasswordHash.Length - derivedKeyLength - saltByteLength];
            Buffer.BlockCopy(actualPasswordHash, 0, actualDerivedKey, 0, derivedKeyLength);
            Buffer.BlockCopy(actualPasswordHash, derivedKeyLength, salt, 0, saltByteLength);
            Buffer.BlockCopy(actualPasswordHash, derivedKeyLength + saltByteLength, iterationCountArray, 0, iterationCountArray.Length);
            byte[] guessedDerivedKey = GenerateHashValue(password, salt, BitConverter.ToInt32(iterationCountArray, 0));
            return PasswordComparison(guessedDerivedKey, actualDerivedKey);
        }

        private static bool PasswordComparison(byte[] guessedHash, byte[] actualHash)
        {
            uint difference = (uint)guessedHash.Length ^ (uint)actualHash.Length;
            for (int i = 0; i < guessedHash.Length && i < actualHash.Length; i++)
            {
                difference |= (uint)(guessedHash[i] ^ actualHash[i]);
            }
            return difference == 0;
        }

        public static byte[] GenerateRandomSalt()
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] salt = new byte[saltByteLength];
            rng.GetBytes(salt);
            return salt;
        }
        public static double RoundToNearest(this double d, double integerPart)
        {
            return Math.Round(d / integerPart) * integerPart;
        }

        public static void EnableLiveSorting(this ICollectionView collectionView, string updatingPropertyName, ListSortDirection direction)
        {
            collectionView.SortDescriptions.Add(new SortDescription(updatingPropertyName, direction));
            var liveShaping = collectionView as ICollectionViewLiveShaping;
            if (liveShaping == null)
                return;
            if (!liveShaping.LiveSortingProperties.Contains(updatingPropertyName))
                liveShaping.LiveSortingProperties.Add(updatingPropertyName);
            liveShaping.IsLiveSorting = true;
        }

        public static void EnableLiveFiltering(this ICollectionView collectionView, Predicate<object> filterDelegate, string updatingPropertyName)
        {
            collectionView.Filter = filterDelegate;
            var liveShaping = collectionView as ICollectionViewLiveShaping;
            if (liveShaping == null)
                return;
            if (!liveShaping.LiveFilteringProperties.Contains(updatingPropertyName))
                liveShaping.LiveFilteringProperties.Add(updatingPropertyName);
            liveShaping.IsLiveFiltering = true;
        }

        public static void FetchItems<T>(this IDataService dataService, SelectableCollection<T> collection, DataChangedHandler handler) where T : IDataIntegrity, INotifyPropertyChanged
        {
            if (handler != null)
                collection.DataChanged -= handler;
            var assemblyQualifiedName = typeof(T).AssemblyQualifiedName;
            int count = dataService.GetContentCount(assemblyQualifiedName);
            if (count == -1)
                return;
            if (collection.Count > 0)
                collection.Clear();
            for (int i = 0; i < count; i++)
            {
                Debug.WriteLine("-Fetching ({0}/{1}) of {2}", i + 1, count, typeof(T).Name);
                collection.Add((T)dataService.GetContent(assemblyQualifiedName, i));
            }
            if (handler != null)
                collection.DataChanged += handler;
        }

        public static void DataChangedHandler<T>(this IDataService dataService, DataChangedEventArgs e)
        {
            var assemblyQualifiedName = typeof(T).AssemblyQualifiedName;
            dynamic sourceItem = e.SourceItem;
            switch (e.Flag)
            {
                case DataChangedFlag.Added:
                    Debug.WriteLine("-Adding {0}", e.SourceItem);
                    dataService.InsertContent(assemblyQualifiedName, (T)sourceItem, e.Index);
                    break;

                case DataChangedFlag.Removed:
                    Debug.WriteLine("-Removing {0}", e.SourceItem);
                    dataService.RemoveContent(assemblyQualifiedName, e.Index);
                    break;

                case DataChangedFlag.Modified:
                    Debug.WriteLine("-Modifying {0}", e.SourceItem);
                    dataService.ModifyContent(assemblyQualifiedName, (T)sourceItem, e.Index);
                    break;

                case DataChangedFlag.Moved:
                    Debug.WriteLine("-Moving {0}", e.SourceItem);
                    dataService.MoveContent(assemblyQualifiedName, e.OldIndex, e.Index);
                    break;

                case DataChangedFlag.Cleared:
                    Debug.WriteLine("-Clearing {0}", e.SourceItem);
                    dataService.ClearContent(assemblyQualifiedName);
                    break;

                case DataChangedFlag.Unchanged:

                default:
                    Debug.WriteLine("-Modifying {0}", e.SourceItem);
                    dataService.ModifyContent(assemblyQualifiedName, (T)sourceItem, e.Index);
                    break;
            }
        }

        public static DateTime GetYearMonth(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }

        public static DateTime GetLastDateBefore(this DateTime dateTime, int months)
        {
            int correctedMonths = dateTime.Month;
            int minusYears = months / 12;
            if (dateTime.Month <= months % 12)
            {
                minusYears++;
                correctedMonths += 12;
            }
            var newYear = dateTime.Year - minusYears;
            var newMonth = correctedMonths - months % 12;
            return new DateTime(newYear, newMonth, DateTime.DaysInMonth(newYear, newMonth));
        }

        public static T2 SafeGetValue<T1, T2>(this Dictionary<T1, T2> dictionary, T1 key)
        {
            T2 value;
            dictionary.TryGetValue(key, out value);
            return value;
        }

        public static double Round(this double d, int decimals)
        {
            return Math.Round(d, decimals);
        }
    }
}