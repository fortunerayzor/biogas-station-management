﻿using System.Windows.Controls;

namespace BiogasStationManagement.Views.Controls
{
    /// <summary>
    /// Interaction logic for EngineForm.xaml
    /// </summary>
    public partial class EngineForm : UserControl
    {
        public EngineForm()
        {
            InitializeComponent();
        }
    }
}