﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace BiogasStationManagement.Views
{
    public static class Helper
    {
        public static void BringIntoTopOfView(this FrameworkElement frameworkElement)
        {
            if (frameworkElement == null)
            {
                throw new ArgumentNullException("frameworkElement");
            }

            var parentScrollViewer = frameworkElement.FindParent<ScrollViewer>();
            if (parentScrollViewer != null)
            {
                parentScrollViewer.ScrollToBottom();
                frameworkElement.BringIntoView();
                parentScrollViewer.ScrollToVerticalOffset(-10d);
            }
        }

        public static T FindParent<T>(this DependencyObject dependencyObject) where T : DependencyObject
        {
            if (dependencyObject == null)
            {
                throw new ArgumentNullException("dependencyObject");
            }

            T parent;

            do
            {
                dependencyObject = VisualTreeHelper.GetParent(dependencyObject);

                if (dependencyObject == null) return null;

                parent = dependencyObject as T;
            } while (parent == null);

            return parent;
        }
    }
}