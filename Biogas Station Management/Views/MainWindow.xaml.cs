﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace BiogasStationManagement.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new ViewModels.Managers.AppliactionManager(MainFrame.Navigate);
            Keyboard.Focus(MainFrame);
        }

        protected override void OnLocationChanged(EventArgs e)
        {
            ClockPopup.HorizontalOffset += 1;
            ClockPopup.HorizontalOffset -= 1;
            base.OnLocationChanged(e);
        }

        private void ClockLabel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ClockPopup.IsOpen = true;
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void MainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            // When the main window resizes (often caused by the touch keyboard being displayed),
            // attempt to bring the element that has focus into view.
            var elementWithFocus = Keyboard.FocusedElement as FrameworkElement;

            if ((elementWithFocus != null) && ((e.PreviousSize.Height > e.NewSize.Height)))
            {
                elementWithFocus.BringIntoTopOfView();
            }
        }

        private void minimizeButton_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void PopupClockLabel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ClockPopup.IsOpen = false;
        }

        private void Window_LayoutUpdated(object sender, EventArgs e)
        {
            ClockPopup.HorizontalOffset += 1;
            ClockPopup.HorizontalOffset -= 1;
        }

        private void WindowTitle_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            WindowState = WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
        }

        private void WindowTitle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        private void WindowTitle_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (e.WidthChanged && e.NewSize.Width > 173d)
                ((Label)sender).Content = "Správa Bioplynové stanice";
            else if (e.WidthChanged && e.NewSize.Width <= 173d)
                ((Label)sender).Content = "Správa BPS";
        }
    }
}