﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace BiogasStationManagement.Views.TouchSupport
{
    public static class TouchKeyboardHelper
    {
        private static readonly bool hasTouchScreen = HasTouchInput();

        private static readonly string virtualKeyboardPath = System.IO.Path.Combine(
                        Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles),
                @"Microsoft Shared\ink\TabTip.exe");

        private static bool enableTouchKeyboard = false;

        private static bool hidingSkipRequested = false;

        private static bool acceptReturn = false;

        public static void DisableTouchKeyboard()
        {
            enableTouchKeyboard = false;
        }

        public static void EnableTouchKeyboard()
        {
            enableTouchKeyboard = true;
        }

        public static void HideTouchKeyboard()
        {
            if (!hasTouchScreen || !enableTouchKeyboard) return;

            if (hidingSkipRequested)
            {
                hidingSkipRequested = false;
                return;
            }

            var nullIntPtr = new IntPtr(0);
            const uint wmSyscommand = 0x0112;
            var scClose = new IntPtr(0xF060);

            var keyboardWnd = FindWindow("IPTip_Main_Window", null);
            if (keyboardWnd != nullIntPtr)
            {
                SendMessage(keyboardWnd, wmSyscommand, scClose, nullIntPtr);
            }
        }

        public static void RequestHidingSkip()
        {
            hidingSkipRequested = true;
        }

        public static void ShowTouchKeyboard()
        {
            if (!hasTouchScreen || !enableTouchKeyboard) return;

            try
            {
                Process.Start(virtualKeyboardPath);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }
        
        public static void HookTouchControls(this Control control)
        {
            // event registration for showing touch input on touching the control
            control.GotTouchCapture += TouchUserControl_GotTouchCapture;
            control.GotKeyboardFocus += TouchUserControl_GotKeyboardFocus;
            control.LostKeyboardFocus += TouchUserControl_LostKeyboardFocus;

            // turn off keyboard on mouse focus
            control.MouseLeftButtonDown += TouchUserControl_MouseLeftButtonDown;

            // tab on enter keypress
            control.PreviewKeyUp += TouchUserControl_PreviewKeyUp;
        }

        private static void TouchUserControl_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (!acceptReturn && e.Key == Key.Return)
            {
                RequestHidingSkip();
                TraversalRequest request = new TraversalRequest(FocusNavigationDirection.Next);
                request.Wrapped = true;
                (e.OriginalSource as UIElement)?.MoveFocus(request);
            }
            else if (e.Key == Key.Tab)
            {
                RequestHidingSkip();
            }
        }

        private static void TouchUserControl_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            var control = sender as TextBox;
            if (control != null)
                acceptReturn = control.AcceptsReturn;
            ShowTouchKeyboard();
            if (enableTouchKeyboard)
                (e.NewFocus as FrameworkElement)?.BringIntoTopOfView();
        }

        private static void TouchUserControl_GotTouchCapture(object sender, System.Windows.Input.TouchEventArgs e)
        {
            EnableTouchKeyboard();
        }

        private static void TouchUserControl_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            acceptReturn = false;
            HideTouchKeyboard();
        }

        private static void TouchUserControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DisableTouchKeyboard();
        }

        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        private static extern IntPtr FindWindow(string sClassName, string sAppName);

        private static bool HasTouchInput()
        {
            return Tablet.TabletDevices.Cast<TabletDevice>().Any(
                tabletDevice => tabletDevice.Type == TabletDeviceType.Touch);
        }

        [DllImport("user32.dll", EntryPoint = "SendMessage", SetLastError = true)]
        private static extern IntPtr SendMessage(IntPtr hWnd, uint uMsg, IntPtr wParam, IntPtr lParam);
    }
}