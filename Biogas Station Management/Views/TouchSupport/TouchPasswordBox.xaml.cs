﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BiogasStationManagement.Views.TouchSupport
{
    /// <summary>
    /// Interaction logic for TouchPasswordBox.xaml
    /// </summary>
    public partial class TouchPasswordBox : UserControl
    {
        public TouchPasswordBox()
        {
            InitializeComponent();
            baseGrid.DataContext = this;
            passwordBox.HookTouchControls();
        }

        public string Password
        {
            get { return (string)GetValue(PasswordProperty); }
            set { SetValue(PasswordProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Password.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PasswordProperty =
            DependencyProperty.Register("Password", typeof(string), typeof(TouchPasswordBox), new FrameworkPropertyMetadata(string.Empty, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, new PropertyChangedCallback(PasswordChangedCallback)));

        private static void PasswordChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var touchPasswordBox = d as TouchPasswordBox;
            if (touchPasswordBox == null)
                return;
            if (touchPasswordBox.passwordBox.Password != (string)e.NewValue)
                touchPasswordBox.passwordBox.Password = (string)e.NewValue;
        }

        private void passwordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Password = ((PasswordBox)sender).Password;
        }
        public void Clear()
        {
            passwordBox.Clear();
        }
    }
}
