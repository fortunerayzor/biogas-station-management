﻿using BiogasStationManagement.Models.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BiogasStationManagement.Models
{
    public class User : IDataIntegrity, INotifyPropertyChanged
    {
        private bool isHandlerDriver;
        private bool isManager;
        private bool isOperator;
        private bool isSystemAdmin;
        private string name;
        private string passwordHash;

        [XmlIgnore]
        public static string AssemblyQualifiedName { get { return typeof(User).AssemblyQualifiedName; } }
        [XmlAttribute]
        public bool IsHandlerDriver
        {
            get { return isHandlerDriver; }
            set
            {
                var lastValue = isHandlerDriver;
                isHandlerDriver = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "IsHandlerDriver", lastValue, isHandlerDriver);
            }
        }
        [XmlAttribute]
        public bool IsManager
        {
            get { return isManager; }
            set
            {
                var lastValue = isManager;
                isManager = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "IsManager", lastValue, isManager);
            }
        }
        [XmlAttribute]
        public bool IsOperator
        {
            get { return isOperator; }
            set
            {
                var lastValue = isOperator;
                isOperator = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "IsOperator", lastValue, isOperator);
            }
        }
        [XmlAttribute]
        public bool IsSystemAdmin
        {
            get { return isSystemAdmin; }
            set
            {
                var lastValue = isSystemAdmin;
                isSystemAdmin= value;
                this.SendDataMessage(DataChanged, PropertyChanged, "IsSystemAdmin", lastValue, isSystemAdmin);
            }
        }
        [XmlAttribute]
        public string Name
        {
            get { return name; }
            set
            {
                var lastValue = name;
                name = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Name", lastValue, name);
            }
        }
        [XmlAttribute]
        public string PasswordHash
        {
            get { return passwordHash; }
            set
            {
                var lastValue = passwordHash;
                passwordHash = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "PasswordHash", lastValue, passwordHash);
            }
        }

        public event DataChangedHandler DataChanged;
        public event PropertyChangedEventHandler PropertyChanged;
    }
}