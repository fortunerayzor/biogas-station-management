﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiogasStationManagement.Models.Data
{
    public interface IDataIntegrity
    {
        event DataChangedHandler DataChanged;
    }
}
