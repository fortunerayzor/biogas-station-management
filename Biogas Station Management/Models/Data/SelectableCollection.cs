﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiogasStationManagement.Models.Data
{
    public class SelectableCollection<T> : ObservableCollection<T>, IDataIntegrity where T : IDataIntegrity, INotifyPropertyChanged
    {
        private T selectedItem;

        public delegate void SelectedItemChangedHandler();

        public event DataChangedHandler DataChanged;

        public event SelectedItemChangedHandler SelectedItemChanged;

        public bool DebugSubscription { get; set; } = false;

        public T SelectedItem
        {
            get
            {
                return selectedItem;
            }
            set
            {
                if (object.ReferenceEquals(selectedItem, value))
                    return;
                selectedItem = value;
                OnPropertyChanged(new PropertyChangedEventArgs("SelectedItem"));
                SelectedItemChanged?.Invoke();
            }
        }

        protected bool DataChangedSubscribed { get { return DataChanged != null; } }
        public override string ToString()
        {
            return string.Format("Type: {0}, Count={1}", typeof(T).Name, Count);
        }

        protected override void ClearItems()
        {
            foreach (T item in this)
            {
                item.DataChanged -= OnDataChanged;
            }
            if (DataChanged != null)
                DataChanged(new DataChangedEventArgs(this, 0, DataChangedFlag.Cleared));
            base.ClearItems();
        }
        protected override void InsertItem(int index, T item)
        {
            base.InsertItem(index, item);
            item.DataChanged += OnDataChanged;
            if (DataChanged != null)
                DataChanged(new DataChangedEventArgs(item, index, DataChangedFlag.Added));
            else if (DebugSubscription)
                System.Diagnostics.Debug.WriteLine("Missing DataChanged subscription (item: {0})", item);
        }

        protected override void MoveItem(int oldIndex, int newIndex)
        {
            T item = this[oldIndex];
            base.MoveItem(oldIndex, newIndex);
            if (DataChanged != null)
                DataChanged(new DataChangedEventArgs(item, newIndex, DataChangedFlag.Moved, oldIndex));
            else if (DebugSubscription)
                System.Diagnostics.Debug.WriteLine("Missing DataChanged subscription (item: {0})", item);
        }

        protected override void RemoveItem(int index)
        {
            T item = this[index];
            base.RemoveItem(index);
            item.DataChanged -= OnDataChanged;
            if (DataChanged != null)
                DataChanged(new DataChangedEventArgs(item, index, DataChangedFlag.Removed));
            else if (DebugSubscription)
                System.Diagnostics.Debug.WriteLine("Missing DataChanged subscription (item: {0})", item);
        }

        protected override void SetItem(int index, T item)
        {
            base.SetItem(index, item);
            if (DataChanged != null)
                DataChanged(new DataChangedEventArgs(item, index, DataChangedFlag.Modified));
            else if (DebugSubscription)
                System.Diagnostics.Debug.WriteLine("Missing DataChanged subscription (item: {0})", item);
        }

        protected virtual void OnDataChanged(DataChangedEventArgs e)
        {
            e.Index = IndexOf((T)e.SourceItem);
            if (DataChanged != null)
                DataChanged(e); // just pass original args and add index
            else if (DebugSubscription)
                System.Diagnostics.Debug.WriteLine("Missing DataChanged subscription (item: {0})", e.SourceItem);
        }
    }
}