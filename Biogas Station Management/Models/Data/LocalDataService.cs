﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace BiogasStationManagement.Models.Data
{
    public class LocalDataService : IDataService
    {
        private readonly string dbPath = Path.Combine(Properties.Settings.Default.DataFolder, Properties.Settings.Default.LocalDataServiceDatabase);
        private Dictionary<string, List<string>> database;

        public LocalDataService()
        {
            database = new Dictionary<string, List<string>>();
            if (File.Exists(dbPath))
            {
                using (FileStream fs = new FileStream(dbPath, FileMode.Open))
                {
                    StreamReader sr = new StreamReader(fs);
                    while (!sr.EndOfStream)
                    {
                        string s = sr.ReadLine();
                        string[] array = s.Split(':');
                        string assemblyName = array[0];
                        array = array[1].Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                        database.Add(assemblyName, array.ToList());
                    }
                }
            }
        }

        public void AddFirstContent(string assemblyName, object data)
        {
            if (IsTypeRegistered(assemblyName))
            {
                string fileName = Math.Abs(DateTime.Now.ToBinary()).ToString();
                database[assemblyName].Insert(0, fileName);
                SerializeObject(fileName, assemblyName, data);
                SaveAbstactList();
                System.Diagnostics.Debug.WriteLine(string.Format("LDS - Added {0} to first position", data));
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(string.Format("{0} Error, method: {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name));
            }
        }

        public void AddLastContent(string assemblyName, object data)
        {
            if (IsTypeRegistered(assemblyName))
            {
                string fileName = Math.Abs(DateTime.Now.ToBinary()).ToString();
                database[assemblyName].Add(fileName);
                SerializeObject(fileName, assemblyName, data);
                SaveAbstactList();
                System.Diagnostics.Debug.WriteLine(string.Format("LDS - Added: {0} to last position", data));
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(string.Format("{0} Error, method: {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name));
            }
        }

        public void ClearContent(string assemblyName)
        {
            if (IsTypeRegistered(assemblyName))
            {
                List<string> list = database[assemblyName];
                for (int i = 0; i < list.Count; i++)
                    RemoveContent(assemblyName, 0);
                SaveAbstactList();
            }
        }

        public void DeregisterType(string assemblyName)
        {
            if (IsTypeRegistered(assemblyName))
            {
                List<string> list = database[assemblyName];
                for (int i = 0; i < list.Count; i++)
                    RemoveContent(assemblyName, 0);
                database.Remove(assemblyName);
                SaveAbstactList();
            }
        }

        public object GetContent(string assemblyName, int index)
        {
            if (IsTypeRegistered(assemblyName))
            {
                List<string> list = database[assemblyName];
                if (index >= 0 && index < list.Count)
                {
                    string fileName = list[index];
                    Type t = Type.GetType(assemblyName, false);
                    if (t != null)
                    {
                        XmlSerializer xmls = new XmlSerializer(t);
                        object o;
                        using (FileStream fs = new FileStream(GetValidPath(fileName), FileMode.Open))
                        {
                            o = xmls.Deserialize(fs);
                        }
                        return o;
                    }
                }
            }
            return null;
        }

        public int GetContentCount(string assemblyName)
        {
            if (IsTypeRegistered(assemblyName))
            {
                return database[assemblyName].Count;
            }
            return -1;
        }

        public void InsertContent(string assemblyName, object data, int index)
        {
            if (IsTypeRegistered(assemblyName))
            {
                string fileName = Math.Abs(DateTime.Now.ToBinary()).ToString();
                database[assemblyName].Insert(index, fileName);
                SerializeObject(fileName, assemblyName, data);
                SaveAbstactList();
                System.Diagnostics.Debug.WriteLine(string.Format("LDS - Inserting {0} to {1} position", data, index));
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(string.Format("{0} Error, method: {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name));
            }
        }

        public bool IsTypeRegistered(string assemblyName)
        {
            return database.ContainsKey(assemblyName);
        }

        public void ModifyContent(string assemblyName, object data, int index)
        {
            if (IsTypeRegistered(assemblyName))
            {
                List<string> list = database[assemblyName];
                if (index >= 0 && index < list.Count)
                {
                    SerializeObject(list[index], assemblyName, data);
                    System.Diagnostics.Debug.WriteLine(string.Format("LDS - Modified: {0}, index: {1}", data, index));
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(string.Format("{0} Error, method: {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name));
            }
        }

        public void MoveContent(string assemblyName, int oldIndex, int newIndex)
        {
            if (IsTypeRegistered(assemblyName))
            {
                List<string> list = database[assemblyName];
                if (oldIndex < list.Count && newIndex < list.Count && oldIndex >= 0 && newIndex >= 0)
                {
                    string oldIndexContent = list.ElementAt(oldIndex);
                    list.RemoveAt(oldIndex);
                    list.Insert(newIndex, oldIndexContent);
                    SaveAbstactList();
                    System.Diagnostics.Debug.WriteLine(string.Format("LDS - Moved: {0}, index: {1}", oldIndex, newIndex));
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(string.Format("{0} Error, method: {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name));
            }
        }

        public void RegisterType(string assemblyName)
        {
            if (!IsTypeRegistered(assemblyName))
            {
                database.Add(assemblyName, new List<string>());
                SaveAbstactList();
            }
        }

        public void RemoveContent(string assemblyName, int index)
        {
            if (IsTypeRegistered(assemblyName))
            {
                List<string> list = database[assemblyName];
                if (index >= 0 && index < list.Count)
                {
                    File.Delete(GetValidPath(list[index]));
                    list.RemoveAt(index);
                    SaveAbstactList();
                    System.Diagnostics.Debug.WriteLine(string.Format("LDS - Removed: index: {0}", index));
                }
            }
        }

        private string GetValidPath(string fileName)
        {
            return Path.Combine(Properties.Settings.Default.DataFolder, fileName);
        }

        private void SaveAbstactList()
        {
            using (StreamWriter sw = new StreamWriter(new FileStream(dbPath, FileMode.Create)))
            {
                foreach (var item in database)
                    sw.WriteLine("{0}:{1}", item.Key, item.Value.Aggregate("", (x, y) => x + y + ";"));
            }
        }

        private void SerializeObject(string fileName, string assemblyName, object data)
        {
            using (FileStream fs = new FileStream(GetValidPath(fileName), FileMode.Create))
            {
                Type t = Type.GetType(assemblyName, false);
                if (t != null)
                {
                    //Add an empty namespace and empty value
                    XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                    ns.Add("", "");
                    XmlSerializer xmls = new XmlSerializer(t);
                    xmls.Serialize(fs, data, ns);
                }
            }
        }
    }
}