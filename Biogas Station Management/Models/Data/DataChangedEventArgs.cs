﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiogasStationManagement.Models.Data
{
    public delegate void DataChangedHandler(DataChangedEventArgs e);

    public enum DataChangedFlag
    {
        Added, Removed, Modified, Moved, Cleared, Unchanged
    }

    public class DataChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Constructor for creating data integrity check.
        /// </summary>
        /// <param name="sourceItem">Item that raises data integrity check</param>
        /// <param name="propertyName">Property which value has changed</param>
        /// <param name="oldValue">Old value</param>
        /// <param name="newValue">New value</param>
        public DataChangedEventArgs(IDataIntegrity sourceItem, string propertyName, object oldValue, object newValue)
        {
            SourceItem = sourceItem;
            PropertyValueType = sourceItem.GetType().GetProperty(propertyName).PropertyType;
            PropertyName = propertyName;
            PropertyOldValue = oldValue;
            PropertyNewValue = newValue;
            Flag = DataChangedFlag.Modified;
        }

        /// <summary>
        /// Constructor that allows a collection to raise an event because of CollectionChanged event.
        /// </summary>
        /// <param name="sourceItem">Item that raises data integrity check</param>
        /// <param name="index">Index where item is inserted or removed from</param>
        /// <param name="flag">Flag signalling certain collection operation</param>
        /// <param name="oldIndex">Used for moving the item</param>
        public DataChangedEventArgs(IDataIntegrity sourceItem, int index, DataChangedFlag flag, int oldIndex = 0)
        {
            SourceItem = sourceItem;
            Index = index;
            OldIndex = oldIndex;
            Flag = flag;
        }

        /// <summary>
        /// Wrapping constructor for rerouting a message from child to parent.
        /// </summary>
        /// <param name="parentSourceItem">Parent item</param>
        /// <param name="childEventArgs">Event arguments of child</param>
        public DataChangedEventArgs(IDataIntegrity parentSourceItem, DataChangedEventArgs childEventArgs)
        {
            SourceItem = parentSourceItem;
            ChildEventArgs = childEventArgs;
            Flag = DataChangedFlag.Unchanged;
        }

        public DataChangedFlag Flag { get; set; }

        public DataChangedEventArgs ChildEventArgs { get; private set; }

        public int Index { get; set; }

        public int OldIndex { get; set; }

        public string PropertyName { get; private set; }

        public object PropertyNewValue { get; private set; }

        public object PropertyOldValue { get; private set; }

        public Type PropertyValueType { get; private set; }

        public IDataIntegrity SourceItem { get; private set; }
    }
}