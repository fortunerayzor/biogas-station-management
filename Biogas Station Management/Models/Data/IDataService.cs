﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiogasStationManagement.Models.Data
{
    /// <summary>
    /// Interface for simple data storage and retrieval should be implemented as virtual list of given registered type.
    /// </summary>
    public interface IDataService
    {
        /// <summary>
        /// Adds content to the abstract list as a first item.
        /// </summary>
        /// <param name="assemblyName">Assembly qualified name</param>
        /// <param name="data">Data to store</param>
        void AddFirstContent(string assemblyName, object data);

        /// <summary>
        /// Adds content to the abstract list as a last item.
        /// </summary>
        /// <param name="assemblyName">Assembly qualified name</param>
        /// <param name="data">Data to store</param>
        void AddLastContent(string assemblyName, object data);

        /// <summary>
        /// Clears the content of given item in abstract list.
        /// </summary>
        /// <param name="assemblyName">Assembly qualified name</param>
        void ClearContent(string assemblyName);

        /// <summary>
        /// Deregisters the type and deletes all relevant data from storage.
        /// </summary>
        /// <param name="assemblyName">Assembly qualified name</param>
        void DeregisterType(string assemblyName);

        /// <summary>
        /// Gets the object of specified type and specified index. Returns null if index is out of range.
        /// </summary>
        /// <param name="assemblyName">Assembly qualified name</param>
        /// <param name="index">Index in virtual list</param>
        /// <returns></returns>
        object GetContent(string assemblyName, int index);

        /// <summary>
        /// Returns the number of items in abstract list of given assembly name.
        /// </summary>
        /// <param name="assemblyName">Assembly qualified name</param>
        /// <returns></returns>
        int GetContentCount(string assemblyName);

        /// <summary>
        /// Inserts content to the specified index.
        /// </summary>
        /// <param name="assemblyName">Assembly qualified name</param>
        /// <param name="data">Data to store</param>
        /// <param name="index">Index in virtual list</param>
        void InsertContent(string assemblyName, object data, int index);

        /// <summary>
        /// Checks if the assembly name is already registered.
        /// </summary>
        /// <param name="assemblyName">Assembly qualified name</param>
        /// <returns></returns>
        bool IsTypeRegistered(string assemblyName);

        /// <summary>
        /// Modifies the content of given item in abstract list with specified index.
        /// </summary>
        /// <param name="assemblyName">Assembly qualified name</param>
        /// <param name="data">Modified data</param>
        /// <param name="index">Index of modified data</param>
        void ModifyContent(string assemblyName, object data, int index);

        /// <summary>
        /// Moves the content of the given item in abstract list by inserting the content from the old index value to the new index.
        /// </summary>
        /// <param name="assemblyName">Assembly qualified name</param>
        /// <param name="oldIndex">Modified data</param>
        /// <param name="newIndex">Index of modified data</param>
        void MoveContent(string assemblyName, int oldIndex, int newIndex);

        /// <summary>
        /// Registers the type and creates abstract list for items.
        /// </summary>
        /// <param name="assemblyName">Assembly qualified name</param>
        void RegisterType(string assemblyName);

        /// <summary>
        /// Deletes the content of given item in abstract list with specified index.
        /// </summary>
        /// <param name="assemblyName">Assembly qualified name</param>
        /// <param name="data">Modified data</param>
        /// <param name="index">Index of modified data</param>
        void RemoveContent(string assemblyName, int index);
    }
}