﻿using BiogasStationManagement.Models.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BiogasStationManagement.Models.Logs
{
    public class Fermenter : IDataIntegrity, INotifyPropertyChanged
    {
        private int mixer1Load;

        private int mixer2Load;

        private int mixingDuration;

        private int pauseDuration;

        public event DataChangedHandler DataChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        [XmlAttribute]
        public int Mixer1Load
        {
            get { return mixer1Load; }
            set
            {
                var lastValue = mixer1Load;
                mixer1Load = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Mixer1Load", lastValue, mixer1Load);
            }
        }

        [XmlAttribute]
        public int Mixer2Load
        {
            get { return mixer2Load; }
            set
            {
                var lastValue = mixer2Load;
                mixer2Load = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Mixer2Load", lastValue, mixer2Load);
            }
        }

        [XmlAttribute]
        public int MixingDuration
        {
            get { return mixingDuration; }
            set
            {
                var lastValue = mixingDuration;
                mixingDuration = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "MixingDuration", lastValue, mixingDuration);
            }
        }

        [XmlAttribute]
        public int PauseDuration
        {
            get { return pauseDuration; }
            set
            {
                var lastValue = pauseDuration;
                pauseDuration = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "PauseDuration", lastValue, pauseDuration);
            }
        }
    }
}