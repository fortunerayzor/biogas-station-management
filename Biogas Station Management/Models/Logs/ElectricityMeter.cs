﻿using BiogasStationManagement.Models.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BiogasStationManagement.Models.Logs
{
    public class ElectricityMeter : IDataIntegrity, INotifyPropertyChanged
    {
        private double consumption;

        private double supply;

        public event DataChangedHandler DataChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        [XmlAttribute]
        public double Consumption
        {
            get { return consumption; }
            set
            {
                var lastValue = consumption;
                consumption = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Consumption", lastValue, consumption);
            }
        }

        [XmlAttribute]
        public double Supply
        {
            get { return supply; }
            set
            {
                var lastValue = supply;
                supply = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Supply", lastValue, supply);
            }
        }
    }
}