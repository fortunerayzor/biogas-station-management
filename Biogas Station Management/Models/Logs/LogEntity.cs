﻿using BiogasStationManagement.Models.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BiogasStationManagement.Models.Logs
{
    public class LogEntity : IDataIntegrity, INotifyPropertyChanged
    {
        private Caliduct caliduct;
        private ElectricityMeter electricityMeter;
        private Engine engine1, engine2, engine3;
        private Fermenter fermenter1, fermenter2;
        private DateTime timestamp;
        private Vielfrass vielfrassDay, vielfrassNight;

        public LogEntity()
        {
        }

        public LogEntity(DateTime timestamp)
        {
            Timestamp = timestamp;
            Caliduct = new Caliduct();
            ElectricityMeter = new ElectricityMeter();
            Engine1 = new Engine();
            Engine2 = new Engine();
            Engine3 = new Engine();
            Fermenter1 = new Fermenter();
            Fermenter2 = new Fermenter();
            VielfrassDay = new Vielfrass();
            VielfrassNight = new Vielfrass();
        }

        public event DataChangedHandler DataChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        [XmlIgnore]
        public static string AssemblyQualifiedName { get { return typeof(LogEntity).AssemblyQualifiedName; } }

        [XmlElement]
        public Caliduct Caliduct
        {
            get
            {
                return caliduct;
            }
            set
            {
                caliduct = value;
                if (caliduct != null)
                    caliduct.DataChanged += dataChanged;
            }
        }

        [XmlElement]
        public ElectricityMeter ElectricityMeter
        {
            get
            {
                return electricityMeter;
            }

            set
            {
                electricityMeter = value;
                if (electricityMeter != null)
                    electricityMeter.DataChanged += dataChanged;
            }
        }

        [XmlElement]
        public Engine Engine1
        {
            get
            {
                return engine1;
            }

            set
            {
                engine1 = value;
                if (engine1 != null)
                    engine1.DataChanged += dataChanged;
            }
        }

        [XmlElement]
        public Engine Engine2
        {
            get
            {
                return engine2;
            }

            set
            {
                engine2 = value;
                if (engine2 != null)
                    engine2.DataChanged += dataChanged;
            }
        }

        [XmlElement]
        public Engine Engine3
        {
            get
            {
                return engine3;
            }

            set
            {
                engine3 = value;
                if (engine3 != null)
                    engine3.DataChanged += dataChanged;
            }
        }

        [XmlElement]
        public Fermenter Fermenter1
        {
            get
            {
                return fermenter1;
            }

            set
            {
                fermenter1 = value;
                if (fermenter1 != null)
                    fermenter1.DataChanged += dataChanged;
            }
        }

        [XmlElement]
        public Fermenter Fermenter2
        {
            get
            {
                return fermenter2;
            }

            set
            {
                fermenter2 = value;
                if (fermenter2 != null)
                    fermenter2.DataChanged += dataChanged;
            }
        }

        [XmlAttribute]
        public DateTime Timestamp
        {
            get { return timestamp; }
            set
            {
                var lastValue = timestamp;
                timestamp = value;
                if (timestamp != null)
                    this.SendDataMessage(DataChanged, PropertyChanged, "Timestamp", lastValue, timestamp);
            }
        }

        [XmlElement]
        public Vielfrass VielfrassDay
        {
            get
            {
                return vielfrassDay;
            }

            set
            {
                vielfrassDay = value;
                if (vielfrassDay != null)
                    vielfrassDay.DataChanged += dataChanged;
            }
        }

        [XmlElement]
        public Vielfrass VielfrassNight
        {
            get
            {
                return vielfrassNight;
            }

            set
            {
                vielfrassNight = value;
                if (vielfrassNight != null)
                    vielfrassNight.DataChanged += dataChanged;
            }
        }

        private void dataChanged(DataChangedEventArgs e)
        {
            DataChanged?.Invoke(new DataChangedEventArgs(this, e));
        }
    }
}