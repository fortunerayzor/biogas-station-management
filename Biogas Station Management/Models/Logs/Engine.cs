﻿using BiogasStationManagement.Models.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BiogasStationManagement.Models.Logs
{
    public class Engine : IDataIntegrity, INotifyPropertyChanged
    {
        private double electricityCounter;
        private double gasConsumption;
        private double gasCounter;
        private double oilConsumption;
        private double oilCounter;
        private double runningHoursCounter;

        public event DataChangedHandler DataChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        [XmlAttribute]
        public double ElectricityCounter
        {
            get
            {
                return electricityCounter;
            }
            set
            {
                var lastValue = electricityCounter;
                electricityCounter = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "ElectricityCounter", lastValue, electricityCounter);
            }
        }

        [XmlAttribute]
        public double GasConsumption
        {
            get
            {
                return gasConsumption;
            }
            set
            {
                var lastValue = gasConsumption;
                gasConsumption = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "GasConsumption", lastValue, gasConsumption);
            }
        }

        [XmlAttribute]
        public double GasCounter
        {
            get
            {
                return gasCounter;
            }
            set
            {
                var lastValue = gasCounter;
                gasCounter = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "GasCounter", lastValue, gasCounter);
            }
        }

        [XmlAttribute]
        public double OilConsumption
        {
            get
            {
                return oilConsumption;
            }
            set
            {
                var lastValue = oilConsumption;
                oilConsumption = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "OilConsumption", lastValue, oilConsumption);
            }
        }

        [XmlAttribute]
        public double OilCounter
        {
            get
            {
                return oilCounter;
            }
            set
            {
                var lastValue = oilCounter;
                oilCounter = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "OilCounter", lastValue, oilCounter);
            }
        }

        [XmlAttribute]
        public double RunningHoursCounter
        {
            get
            {
                return runningHoursCounter;
            }
            set
            {
                var lastValue = runningHoursCounter;
                runningHoursCounter = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "RunningHoursCounter", lastValue, runningHoursCounter);
            }
        }
    }
}