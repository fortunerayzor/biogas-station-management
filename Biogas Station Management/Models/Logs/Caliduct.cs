﻿using BiogasStationManagement.Models.Data;
using System.ComponentModel;
using System.Xml.Serialization;

namespace BiogasStationManagement.Models.Logs
{
    public class Caliduct : IDataIntegrity, INotifyPropertyChanged
    {
        private double energyCounter;

        private double flow;

        private double inletTemp;

        private double outletTemp;

        private double power;

        private double pressure;

        private double suppliedWaterCounter;

        public event DataChangedHandler DataChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        [XmlAttribute]
        public double EnergyCounter
        {
            get { return energyCounter; }
            set
            {
                var lastValue = energyCounter;
                energyCounter = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "EnergyCounter", lastValue, energyCounter);
            }
        }

        [XmlAttribute]
        public double Flow
        {
            get { return flow; }
            set
            {
                var lastValue = flow;
                flow = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Flow", lastValue, flow);
            }
        }

        [XmlAttribute]
        public double InletTemp
        {
            get { return inletTemp; }
            set
            {
                var lastValue = inletTemp;
                inletTemp = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "InletTemp", lastValue, inletTemp);
            }
        }

        [XmlAttribute]
        public double OutletTemp
        {
            get { return outletTemp; }
            set
            {
                var lastValue = outletTemp;
                outletTemp = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "OutletTemp", lastValue, outletTemp);
            }
        }

        [XmlAttribute]
        public double Power
        {
            get { return power; }
            set
            {
                var lastValue = power;
                power = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Power", lastValue, power);
            }
        }

        [XmlAttribute]
        public double Pressure
        {
            get { return pressure; }
            set
            {
                var lastValue = pressure;
                pressure = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Pressure", lastValue, pressure);
            }
        }

        [XmlAttribute]
        public double SuppliedWaterCounter
        {
            get { return suppliedWaterCounter; }
            set
            {
                var lastValue = suppliedWaterCounter;
                suppliedWaterCounter = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "SuppliedWaterCounter", lastValue, suppliedWaterCounter);
            }
        }
    }
}