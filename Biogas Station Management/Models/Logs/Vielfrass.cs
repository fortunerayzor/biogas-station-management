﻿using BiogasStationManagement.Models.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BiogasStationManagement.Models.Logs
{
    public class Vielfrass : IDataIntegrity, INotifyPropertyChanged
    {
        private int cycleCount;

        private int duration;

        public event DataChangedHandler DataChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        [XmlAttribute]
        public int CycleCount
        {
            get { return cycleCount; }
            set
            {
                var lastValue = cycleCount;
                cycleCount = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "CycleCount", lastValue, cycleCount);
            }
        }

        [XmlAttribute]
        public int Duration
        {
            get { return duration; }
            set
            {
                var lastValue = duration;
                duration = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Duration", lastValue, duration);
            }
        }
    }
}