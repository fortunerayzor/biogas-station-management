﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using BiogasStationManagement.Models.Data;

namespace BiogasStationManagement.Models.Checks
{
    public class IrregularCheckHistory : IDataIntegrity, INotifyPropertyChanged
    {
        private SelectableCollection<Check> checks;
        private string name;

        public IrregularCheckHistory()
        {
            Checks = new SelectableCollection<Check>();
        }

        public event DataChangedHandler DataChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        [XmlIgnore]
        public static string AssemblyQualifiedName { get { return typeof(IrregularCheckHistory).AssemblyQualifiedName; } }

        [XmlArray]
        public SelectableCollection<Check> Checks
        {
            get
            {
                return checks;
            }
            set
            {
                if (checks != null)
                    checks.DataChanged -= Checks_DataChanged;
                checks = value;
                if (checks != null)
                    checks.DataChanged += Checks_DataChanged;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Checks"));
            }
        }

        [XmlAttribute]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                var oldValue = name;
                name = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Name", oldValue, name);
            }
        }

        private void Checks_DataChanged(DataChangedEventArgs e)
        {
            DataChanged?.Invoke(new DataChangedEventArgs(this, e));
        }
    }
}