﻿using BiogasStationManagement.Models.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BiogasStationManagement.Models.Checks
{
    public class Check : IDataIntegrity, INotifyPropertyChanged
    {
        private string note;
        private DateTime timestamp;

        public event DataChangedHandler DataChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        [XmlAttribute]
        public string Notes
        {
            get { return note; }
            set
            {
                var oldValue = note;
                note = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Notes", oldValue, note);
            }
        }

        [XmlAttribute]
        public DateTime Timestamp
        {
            get { return timestamp; }
            set
            {
                var oldValue = timestamp;
                timestamp = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Timestamp", oldValue, timestamp);
            }
        }
    }
}