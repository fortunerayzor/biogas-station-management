﻿using BiogasStationManagement.Models.Data;
using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace BiogasStationManagement.Models.Checks
{
    public class EngineCheck : IDataIntegrity, INotifyPropertyChanged
    {
        private double engineHours;
        private string notes;
        private DateTime timestamp;

        public event DataChangedHandler DataChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        [XmlAttribute]
        public double EngineHours
        {
            get { return engineHours; }
            set
            {
                var oldValue = engineHours;
                engineHours = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "EngineHours", oldValue, engineHours);
            }
        }

        [XmlAttribute]
        public string Notes
        {
            get { return notes; }
            set
            {
                var oldValue = notes;
                notes = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Note", oldValue, notes);
            }
        }

        [XmlAttribute]
        public DateTime Timestamp
        {
            get { return timestamp; }
            set
            {
                var oldValue = timestamp;
                timestamp = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Timestamp", oldValue, timestamp);
            }
        }
    }
}