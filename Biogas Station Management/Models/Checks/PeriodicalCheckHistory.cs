﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using BiogasStationManagement.Models.Data;

namespace BiogasStationManagement.Models.Checks
{
    public class PeriodicalCheckHistory : IDataIntegrity, INotifyPropertyChanged
    {
        private DateTime firstCheck;
        private SelectableCollection<Check> checks;
        private string name;

        private TimeSpan periodicity;

        public PeriodicalCheckHistory()
        {
            Checks = new SelectableCollection<Check>();
        }

        public event DataChangedHandler DataChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        [XmlIgnore]
        public static string AssemblyQualifiedName { get { return typeof(PeriodicalCheckHistory).AssemblyQualifiedName; } }

        [XmlAttribute]
        public DateTime FirstCheck
        {
            get
            {
                return firstCheck;
            }
            set
            {
                var oldValue = firstCheck;
                firstCheck = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "FirstCheck", oldValue, firstCheck);
            }
        }

        [XmlArray]
        public SelectableCollection<Check> Checks
        {
            get
            {
                return checks;
            }
            set
            {
                if (checks != null)
                    checks.DataChanged -= Checks_DataChanged;
                checks = value;
                if (checks != null)
                    checks.DataChanged += Checks_DataChanged;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Checks"));
            }
        }

        [XmlAttribute]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                var oldValue = name;
                name = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Name", oldValue, name);
            }
        }

        [XmlIgnore]
        public TimeSpan Periodicity
        {
            get
            {
                return periodicity;
            }
            set
            {
                var oldValue = periodicity;
                periodicity = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Periodicity", oldValue, periodicity);
            }
        }

        [Browsable(false)]
        [XmlElement(DataType = "duration", ElementName = "Periodicity")]
        public string PeriodicityString
        {
            get
            {
                return XmlConvert.ToString(Periodicity);
            }
            set
            {
                Periodicity = string.IsNullOrEmpty(value) ? TimeSpan.Zero : XmlConvert.ToTimeSpan(value);
            }
        }

        private void Checks_DataChanged(DataChangedEventArgs e)
        {
            DataChanged?.Invoke(new DataChangedEventArgs(this, e));
        }
    }
}