﻿using BiogasStationManagement.Models.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BiogasStationManagement.Models.Checks
{
    public class EngineCheckHistory : IDataIntegrity, INotifyPropertyChanged
    {
        private int engineIndex;
        private SelectableCollection<EngineCheck> checks;

        private bool addIntervalFromLastCheck;

        private string name;
        private double serviceInterval;

        public event DataChangedHandler DataChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        public EngineCheckHistory()
        {
            EngineChecks = new SelectableCollection<EngineCheck>();
        }
        
        [XmlIgnore]
        public static string AssemblyQualifiedName { get { return typeof(EngineCheckHistory).AssemblyQualifiedName; } }

        [XmlAttribute]
        public int EngineIndex
        {
            get { return engineIndex; }
            set
            {
                var oldValue = engineIndex;
                engineIndex = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "EngineIndex", oldValue, engineIndex);
            }
        }

        [XmlArray]
        public SelectableCollection<EngineCheck> EngineChecks
        {
            get { return checks; }
            set
            {
                if(checks != null)
                    checks.DataChanged -= Checks_DataChanged;
                checks = value;
                if (checks != null)
                    checks.DataChanged += Checks_DataChanged;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Checks"));
            }
        }

        [XmlAttribute]
        public bool AddIntervalFromLastCheck
        {
            get { return addIntervalFromLastCheck; }
            set
            {
                var oldValue = addIntervalFromLastCheck;
                addIntervalFromLastCheck = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "AddIntervalFromLastCheck", oldValue, addIntervalFromLastCheck);
            }
        }

        [XmlAttribute]
        public string Name
        {
            get { return name; }
            set
            {
                var oldValue = name;
                name = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Name", oldValue, name);
            }
        }

        [XmlAttribute]
        public double ServiceInterval
        {
            get { return serviceInterval; }
            set
            {
                var oldValue = serviceInterval;
                serviceInterval = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "ServiceInterval", oldValue, serviceInterval);
            }
        }

        private void Checks_DataChanged(DataChangedEventArgs e)
        {
            DataChanged?.Invoke(new DataChangedEventArgs(this, e));
        }
    }
}