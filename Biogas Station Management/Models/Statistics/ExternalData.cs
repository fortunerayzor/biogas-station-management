﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using BiogasStationManagement.Models.Data;

namespace BiogasStationManagement.Models.Statistics
{
    public class ExternalData : IDataIntegrity, INotifyPropertyChanged
    {
        private double electricityNettoDistribution;
        private double heatConsumption;
        private double heatExternalSupply;
        private double heatInternalSupply;
        private double methanePercentage;
        private double oilDensity;
        private DateTime timestamp;

        public event DataChangedHandler DataChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        [XmlIgnore]
        public static string AssemblyQualifiedName { get { return typeof(ExternalData).AssemblyQualifiedName; } }

        [XmlAttribute]
        public double ElectricityNettoDistribution
        {
            get
            {
                return electricityNettoDistribution;
            }
            set
            {
                var oldValue = electricityNettoDistribution;
                electricityNettoDistribution = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "ElectricityNettoDistribution", oldValue, value);
            }
        }

        [XmlAttribute]
        public double HeatConsumption
        {
            get
            {
                return heatConsumption;
            }
            set
            {
                var oldValue = heatConsumption;
                heatConsumption = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "HeatConsumption", oldValue, value);
            }
        }

        [XmlAttribute]
        public double HeatExternalSupply
        {
            get
            {
                return heatExternalSupply;
            }
            set
            {
                var oldValue = heatExternalSupply;
                heatExternalSupply = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "HeatExternalSupply", oldValue, value);
            }
        }

        [XmlAttribute]
        public double HeatInternalSupply
        {
            get
            {
                return heatInternalSupply;
            }
            set
            {
                var oldValue = heatInternalSupply;
                heatInternalSupply = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "HeatInternalSupply", oldValue, value);
            }
        }

        [XmlAttribute]
        public double MethanePercentage
        {
            get
            {
                return methanePercentage;
            }
            set
            {
                var oldValue = methanePercentage; methanePercentage = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "MethanePercentage", oldValue, value);
            }
        }

        [XmlAttribute]
        public double OilDensity
        {
            get
            {
                return oilDensity;
            }
            set
            {
                var oldValue = oilDensity;
                oilDensity = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "OilDensity", oldValue, value);
            }
        }

        [XmlAttribute]
        public DateTime Timestamp
        {
            get
            {
                return timestamp;
            }
            set
            {
                var oldValue = timestamp;
                timestamp = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Timestamp", oldValue, value);
            }
        }
    }
}