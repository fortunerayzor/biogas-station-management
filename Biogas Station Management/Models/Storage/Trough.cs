﻿using BiogasStationManagement.Models.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BiogasStationManagement.Models.Storage
{
    public class Trough : IDataIntegrity, INotifyPropertyChanged
    {
        private string description;
        private bool isLocked;
        private SelectableCollection<Load> loads;
        private string storageName;
        private int troughIndex;

        public Trough()
        {
            loads = new SelectableCollection<Load>();
        }

        public event DataChangedHandler DataChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        [XmlIgnore]
        public static string AssemblyQualifiedName { get { return typeof(Trough).AssemblyQualifiedName; } }

        [XmlAttribute]
        public string Description
        {
            get { return description; }
            set
            {
                var oldValue = description;
                description = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Description", oldValue, description);
            }
        }

        [XmlAttribute]
        public bool IsLocked
        {
            get { return isLocked; }
            set
            {
                var oldValue = isLocked;
                isLocked = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "IsLocked", oldValue, isLocked);
            }
        }

        [XmlArray]
        public SelectableCollection<Load> Loads
        {
            get { return loads; }
            set
            {
                loads = value;
                if (loads != null)
                    loads.DataChanged += Loads_DataChanged;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Loads"));
            }
        }

        [XmlAttribute]
        public string StorageName
        {
            get { return storageName; }
            set
            {
                var oldValue = storageName;
                storageName = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "StorageName", oldValue, storageName);
            }
        }

        [XmlAttribute]
        public int TroughIndex
        {
            get { return troughIndex; }
            set
            {
                var oldValue = troughIndex;
                troughIndex = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "TroughIndex", oldValue, troughIndex);
            }
        }

        private void Loads_DataChanged(DataChangedEventArgs e)
        {
            DataChanged?.Invoke(new DataChangedEventArgs(this, e));
        }
    }
}