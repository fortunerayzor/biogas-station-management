﻿using BiogasStationManagement.Models.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BiogasStationManagement.Models.Storage
{
    public class Ingredient : IDataIntegrity, INotifyPropertyChanged
    {
        private double amount;

        private int troughIndex;

        public event DataChangedHandler DataChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        [XmlAttribute]
        public double Amount
        {
            get { return amount; }
            set
            {
                var oldValue = amount;
                amount = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Amount", oldValue, amount);
            }
        }

        [XmlAttribute]
        public int TroughIndex
        {
            get { return troughIndex; }
            set
            {
                var oldValue = troughIndex;
                troughIndex = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "TroughIndex", oldValue, troughIndex);
            }
        }
    }
}