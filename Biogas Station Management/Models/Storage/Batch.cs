﻿using BiogasStationManagement.Models.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BiogasStationManagement.Models.Storage
{
    public class Batch : IDataIntegrity, INotifyPropertyChanged
    {
        private SelectableCollection<Ingredient> ingredients;

        private DateTime timestamp;

        public Batch()
        {
        }

        public Batch(DateTime timestamp)
        {
            Timestamp = timestamp;
            ingredients = new SelectableCollection<Ingredient>();
        }

        public event DataChangedHandler DataChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        [XmlIgnore]
        public static string AssemblyQualifiedName { get { return typeof(Batch).AssemblyQualifiedName; } }

        [XmlArray]
        public SelectableCollection<Ingredient> Ingredients
        {
            get
            {
                return ingredients;
            }

            set
            {
                ingredients = value;
                if (ingredients != null)
                    ingredients.DataChanged += Ingredients_DataChanged;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Ingredients"));
            }
        }

        public DateTime Timestamp
        {
            get { return timestamp; }
            set
            {
                var oldValue = timestamp;
                timestamp = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Timestamp", oldValue, timestamp);
            }
        }

        private void Ingredients_DataChanged(DataChangedEventArgs e)
        {
            DataChanged?.Invoke(new DataChangedEventArgs(this, e));
        }
    }
}