﻿using BiogasStationManagement.Models.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BiogasStationManagement.Models.Storage
{
    public class Load : IDataIntegrity, INotifyPropertyChanged
    {
        private string description;
        private DateTime timestamp;
        private double weight;

        public event DataChangedHandler DataChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        [XmlAttribute]
        public string Description
        {
            get { return description; }
            set
            {
                var oldValue = description;
                description = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Description", oldValue, description);
            }
        }

        [XmlAttribute]
        public DateTime Timestamp
        {
            get { return timestamp; }
            set
            {
                var oldValue = timestamp;
                timestamp = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Timestamp", oldValue, timestamp);
            }
        }

        [XmlAttribute]
        public double Weight
        {
            get { return weight; }
            set
            {
                var oldValue = weight;
                weight = value;
                this.SendDataMessage(DataChanged, PropertyChanged, "Weight", oldValue, weight);
            }
        }
    }
}