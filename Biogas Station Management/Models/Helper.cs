﻿using BiogasStationManagement.Models.Data;
using System.ComponentModel;

namespace BiogasStationManagement.Models
{
    internal static class Helper
    {
        public static void SendDataMessage<T>(this IDataIntegrity sender, DataChangedHandler DataChanged, PropertyChangedEventHandler PropertyChanged, string propertyName, T oldValue, T newValue)
        {
            DataChanged?.Invoke(new DataChangedEventArgs(sender, propertyName, oldValue, newValue));
            PropertyChanged?.Invoke(sender, new PropertyChangedEventArgs(propertyName));
        }
    }
}